# Copyright (c) 2001-2012, Archaeopteryx Software, Inc.  All rights reserved.
#
# Written by John P. Ehresman and Stephan R.A. Deibel
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import glob
import os
import subprocess
import sys
import zipfile

from buildruntimes import projbuilder
from buildruntimes import utils

class PythonBuild(projbuilder.RuntimeBuilder):
    """ This builds a range of runtimes on unix-like (non-win32) platforms, 
    all with the prefix pythonX.X where X.X is the major.minor version of
    python.  On OS X, a universalsdk build for intel x32 & x64 is used.
    """

    SRC_URLS = {
        '2.7': 'http://python.org/ftp/python/2.7.6/Python-2.7.6.tgz',
        '3.2': 'http://python.org/ftp/python/3.2.3/Python-3.2.3.tar.bz2',
        '3.3': 'http://python.org/ftp/python/3.3.2/Python-3.3.2.tar.bz2',
        }

    _separate_debug_runtime_dir = True

    prefix = 'python'
    
    def __init__(self, config, name):

        assert name.startswith(self.prefix)

        projbuilder.RuntimeBuilder.__init__(self, config, name=name)

        self.name = name

        if sys.platform == 'win32':
            # XXX name is python.exe; need to get the version somehow
            self.version = name[len(self.prefix):][:3]
        else:
            self.version = name[len(self.prefix):][:3]

        self.use_openssl_dir_configure_option = False
        use_ssl_rt = self.GetOption('use-openssl-runtime', default=False)
        if use_ssl_rt == 'yes' and sys.platform != 'win32':
            self._dependencies += ('openssl',)
            if self.version >= '3.6':
                self.use_openssl_dir_configure_option = True

        if self.name != 'python' + self.version:
            self._ini_sections[2:2] = [
                'python' + self.version + ':' + self.config.PLATFORM,
                'python' + self.version,
            ]        

        self.url = self.GetOption('source-tarball-url', 
                                  default=self.SRC_URLS.get(self.version))
        if self.url:
            self.local_dist_name = self.url.split('/')[-1]
            self.dist_file = self.GetOption('root') + '/python/' + self.local_dist_name
        else:
            self.local_dist_name = None
            self.dist_file = None
            
            
    def _GetSrcDir(self):
        
        local_dirname = utils.StripExt(self.local_dist_name)
        build_dir = self.GetOption('root') + '/python/' + self.name

        suffix = ''
        if self._GetDebugBuild():
            suffix += '-pydebug'
        if sys.platform == 'darwin':
            suffix += '-framework'

        if suffix:
            build_dir += '-with' + suffix
        return build_dir

    def _Configure(self):

        if sys.platform == 'win32':
            return

        versioned_name = 'python' + self.version
        py_runtime = self._GetRuntimeDir()
        src_dir = self._GetSrcDir()
        env = None
        
        conf_argv = ['sh', 'configure']
        conf_argv += ['-prefix', py_runtime.replace('/', os.sep)]
        if self._GetDebugBuild():
            conf_argv += ['--with-pydebug']

        if sys.platform != 'darwin':
            conf_argv += ['--disable-shared']
        else:
            import platform

            conf_argv += ['--enable-framework=%s/lib' % py_runtime]
            # universalsdk probably does't work before 2.6 and has problems
            # with 3.0; on 3.0, '--with-universal-archs=intel is unknown
            # and other options try to build for ppc, which fail w/ newer
            # SDK's that have dropped ppc support
            if self.version >= '2.6' and self.version != '3.0':
                env = os.environ.copy()
                
                target = self.GetOption('macosx-deployment-target')
                if target is None:
                    mac_ver = platform.mac_ver()
                    ver_parts = mac_ver[0].split('.')
                    target = '.'.join(ver_parts[:2])
                    
                env['MACOSX_DEPLOYMENT_TARGET'] = target

        if 'openssl' in self._dependencies:
            openssl_builder =  projbuilder.GetBuilderForName('openssl', self.config)
            openssl_rt_dir = openssl_builder._GetRuntimeDir()
            if os.path.exists(openssl_rt_dir):
                if self.use_openssl_dir_configure_option:
                    conf_argv += ['--with-openssl=' + openssl_rt_dir]
                else:
                    conf_argv.append('CPPFLAGS=-I%s/include' % (openssl_rt_dir,))
                    conf_argv.append('LDFLAGS=-L%s/lib' % (openssl_rt_dir,))

        if self.GetOption('include-pip', default='no') == 'yes':
            conf_argv += ['--with-ensurepip']
        else:
            conf_argv += ['--without-ensurepip']
                
        self._RunArgvInEnv(conf_argv, rundir=src_dir, env=env)


    def _CopyFromDistribution(self):
        """ Copy the tree from the directory specified by copy-from-distribution
        into the runtime directory. """

        from_dist = self.GetOption('copy-from-distribution')
        if from_dist is None:
            return False

        if self._GetDebugBuild() and sys.platform != 'win32':
            print 'Copying from distribution not supported for debug builds'
            return False

        if sys.platform == 'darwin':
            self._CopyFromDistributionOnDarwin(from_dist)
            return
        if sys.platform == 'win32':
            self._CopyFromDistributionOnWin32(from_dist)
            return

        runtime_dir = self._GetRuntimeDir()
        utils.Install(from_dist, runtime_dir)
                
        return True

    def _CopyFromDistributionOnWin32(self, from_dist):
        """ Copy the tree on win32, copying the PythonXX.dll and .zip into
        runtime. """

        is_debug = self._GetDebugBuild()
        def filter_func(src):
            if src.endswith(('.pdb', '.dll', '.exe', '.pyd')):
                ext = src[-4:]
                under_d = (src[-6:-4] == '_d')
                if not is_debug and (under_d or ext == '.pdb'):
                    return False
                if is_debug and not under_d:
                    with_under_d = src[:-4] + '_d' + src[-4:]
                    if os.path.exists(with_under_d):
                        return False
                
            return True
            
        runtime_dir = self._GetRuntimeDir()
        utils.Install(from_dist, runtime_dir, filter_func=filter_func)

        win_sys_dir = r'c:\windows\system32'
        major, minor = self.version.split('.')
        for ext in ['.dll', '.zip']:
            if is_debug:
                ext = '_d' + ext
            name = ('Python%s%s' % (major, minor)) + ext
            src = win_sys_dir + '/' + name
            if os.path.exists(src):
                utils.Install(src, runtime_dir + '/' + name)

    def _CopyFromDistributionOnDarwin(self, from_dist):
        """ Modify the copy of python on darwin / OS X so that it's
        relocatable. """
        
        # Tested with python2.5; may need tweaking for other versions
        
        runtime_dir = self._GetRuntimeDir()

        framework_top = runtime_dir + '/lib/Python.framework'
        
        # Move everything to framework top and version dir
        version_dest = framework_top + '/Versions/' + self.version
        utils.Install(from_dist, version_dest)

        # Link Current to version
        link_dest = framework_top + '/Versions/Current'
        utils.SymLink(self.version, link_dest)
            
        framework_bin = version_dest + '/bin'
        bin_dir = runtime_dir + '/bin'
        if not os.path.isdir(bin_dir):
            os.mkdir(bin_dir)
        relative_dir = '../' + framework_bin[len(runtime_dir) + 1:]
        for name in os.listdir(framework_bin):
            link_name = bin_dir + '/' + name
            target = relative_dir + '/' + name
            utils.SymLink(target, link_name)
            
        # Link python* executables to real Python executable
        target = '../Resources/Python.app/Contents/MacOS/Python'
        name_list = os.listdir(framework_bin)
        for name in name_list:
            full_name = os.path.join(framework_bin, name)
            if (name.startswith('python') and name.endswith(self.version)
                    and not os.path.islink(full_name)):
                print name
                os.unlink(full_name)
                utils.SymLink(target, full_name)
            
        # Change the Resources/Python.app/Contents/MacOS/Python dependency
        # on the framework shared lib from absolute to relative
        curr_dep = from_dist + '/Python'
        new_dep = '@loader_path/../../../../Python'
        exec_name = version_dest + '/Resources/Python.app/Contents/MacOS/Python'
        argv = ['install_name_tool', '-change', curr_dep, new_dep, exec_name]
        utils.RunArgv(argv, version_dest)
        
        def change_dep_name(exec_name, curr_dep, new_dep):
            argv = ['install_name_tool', '-change', curr_dep, new_dep, exec_name]
            mode = os.stat(exec_name).st_mode
            os.chmod(exec_name, 0o644)
            utils.RunArgv(argv, os.path.dirname(exec_name))
            os.chmod(exec_name, mode)
        
        def get_dep_lib_list(exec_name):
        
            argv = ['otool', '-L', name]
            output = subprocess.check_output(argv)

            line_list = output.splitlines()
            if not line_list[0].startswith(name):
                print 'First line is not file name, output:\n' + output
                return []
   
            dep_lib_list = []             
            for line in line_list[1:]:
                parts = line.split(' ')
                dep_lib = parts[0].strip()
                dep_lib_list.append(dep_lib)
            return dep_lib_list
        
        # In ./lib, there are some .dylib files with links to themselves
        # and other files in the same dir -- in 2.5 at least
        version_dest_lib = version_dest + '/lib'
        for dirname, subdir_list, sub_name_list in os.walk(version_dest_lib):
            name_list = [dirname + '/' + n for n in sub_name_list]
            exec_list = [n for n in name_list if not os.path.islink(n) and n.endswith(('.so', '.dylib'))]
            for name in exec_list:
                dep_lib_list = get_dep_lib_list(name)
                for dep_lib in dep_lib_list:
                    if dep_lib.startswith(from_dist):
                        relative_to_dist = dep_lib[len(from_dist+'/'):]
                        if os.path.dirname(relative_to_dist) == 'lib':
                            relative_dir = dirname[len(version_dest_lib + '/'):]
                            if relative_dir == '':
                                new_dep = '@loader_path/' + os.path.basename(dep_lib)
                            else:
                                slash_count = len(relative_dir.split('/'))
                                new_dep = '@loader_path/' + ('../' * slash_count) + os.path.basename(dep_lib)
                            change_dep_name(name, dep_lib, new_dep)

    def Build(self):

        from_dist = self.GetOption('copy-from-distribution')
        # Can't copy from dist on mac if debug build
        if sys.platform == 'darwin' and self._GetDebugBuild():
            from_dist = None
        if from_dist is not None:
            self._CopyFromDistribution()
            return
    
        build_dir = self._GetSrcDir()
        py_runtime = self._GetRuntimeDir()

        self._UnpackSource()

        if sys.platform == 'win32':
            raise NotImplementedError()
        else:
            if not os.path.exists(build_dir + '/Makefile'):
                self._Configure()

            make_argv = ['make', '-j2']
            self._RunArgvInEnv(make_argv, build_dir)
            make_argv = ['make', 'install']
            if sys.platform == 'darwin':
                # The makefiles install apps into /Applications unless the
                # make variable is overridden
                make_argv.append('PYTHONAPPSDIR=%s/Applications' % py_runtime)
                
            self._RunArgvInEnv(make_argv, build_dir)

            self._ConvertLinksToRelative(py_runtime)

    def _UnpackSource(self):

        build_dir = self._GetSrcDir()
        if os.path.exists(build_dir):
            return
        
        if self.dist_file is None:
            self._DownloadSource()
            return
            
        if not os.path.exists(self.dist_file):
            retr = utils.RetrievalOperations(self.config)
            retr.Download(self.url, self.dist_file)
        utils.UnpackTar(self.dist_file, build_dir)
        
        self._ApplyPatch()        
        
    def Clean(self):

        from_dist = self.GetOption('copy-from-distribution')
        if from_dist is not None:
            return

        build_dir = self._GetSrcDir()
        if not os.path.exists(build_dir):
            return

        if sys.platform == 'win32':
            raise NotImplementedError()
        else:
            if not os.path.exists(build_dir + '/Makefile'):
                return

            make_argv = ['make', 'distclean']
            self._RunArgvInEnv(make_argv, build_dir)

    def _ConvertLinksToRelative(self, dirname):

        def visitlink(fullpath):
            dst = os.readlink(fullpath)
            if not os.path.isabs(dst):
                return

            relative = os.path.relpath(dst, os.path.dirname(fullpath))
            os.remove(fullpath)
            os.symlink(relative, fullpath)

        for dirpath, dirnames, filenames in os.walk(dirname):
            for name in filenames + dirnames:
                fullpath = os.path.join(dirpath, name)
                if os.path.islink(fullpath):
                    visitlink(fullpath)
                    
    def _GetTargetPython(self):
        
        py_runtime = self._GetRuntimeDir()
        
        if sys.platform == 'win32':
            executable = py_runtime + ('\\python_d.exe' if self._GetDebugBuild()
                                       else '\\python.exe')
        else:
            executable = py_runtime + '/bin/python' + self.version
            
        return self.PythonInfo(executable=executable, version=self.version,
                               proj_name=self.name, runtime_suffix='py'+self.version)

class Win32Config:
    
  VS7_DIR = r'c:\Program Files\Microsoft Visual Studio .NET 2003'
  VS7_DEVENV_COM = VS7_DIR + r'\Common7\IDE\devenv.com'
  
  SDK_DIR = r'C:\Program Files\Microsoft Platform SDK for Windows Server 2003 R2'
  SDK_SETENV_CMD = SDK_DIR + r'\SetEnv.Cmd'

  VC7_VCVARS32_BAT = VS7_DIR + r'\VC7\bin\vcvars32.bat'
  
  VS9_DIR = utils.InProgramFiles(r'Microsoft Visual Studio 9.0')
  VS9_DEVENV_COM = utils.InProgramFiles(r'Microsoft Visual Studio 9.0\Common7\IDE\devenv.com')
  VS9_MT = utils.InProgramFiles(r"Microsoft SDKs\Windows\v6.0A\bin\mt.exe")
  VC9_VCVARS32_BAT = utils.InProgramFiles(r'Microsoft Visual Studio 9.0\VC\vcvarsall.bat')
  
  VS10_DEVENV_COM = utils.InProgramFiles(r'Microsoft Visual Studio 10.0\Common7\IDE\devenv.com')
  VS10_VCVARS32_BAT = utils.InProgramFiles(r'Microsoft Visual Studio 10.0\VC\vcvarsall.bat')

  VS11_DEVENV_COM = utils.InProgramFiles(r'Microsoft Visual Studio 11.0\Common7\IDE\devenv.com')
  VS11_VCVARS32_BAT = utils.InProgramFiles(r'Microsoft Visual Studio 11.0\VC\vcvarsall.bat')

  VS12_DEVENV_COM = utils.InProgramFiles(r'Microsoft Visual Studio 12.0\Common7\IDE\devenv.com')
  VS12_VCVARS32_BAT = utils.InProgramFiles(r'Microsoft Visual Studio 12.0\VC\vcvarsall.bat')

  VS14_DEVENV_COM = utils.InProgramFiles(r'Microsoft Visual Studio 14.0\Common7\IDE\devenv.com')
  VS14_VCVARS32_BAT = utils.InProgramFiles(r'Microsoft Visual Studio 14.0\VC\vcvarsall.bat')

class Win32PythonBuild(PythonBuild):
    """ This builds a range of runtimes on win32, all with the prefix pythonX.X where
    X.X is the major.minor version of python (e.g. 2.7).  Suffixes can be added to this:
      -x64 -- build 64 bit / amd64 version
      -vs2010 -- upgrade project file to VS 2010 and build; note that the
                 project is upgraded via the /upgrade devenv command line
                 option and the build may not fully work (but the basic
                 interpreter seems to work)
      -vs2012 -- upgrade project file to VS 2012 and build; note that the
                 project is upgraded via the /upgrade devenv command line
                 option and the build may not fully work (but the basic
                 interpreter seems to work).  A dialog does come up
                 asking for permission to overwrite.  Upgrade may fail
                 with a error message similiar to:
                   No exports were found that match the constraint: ContractName Microsoft.VisualStudio.Project.IProjectServiceAccessor
                 Work around is to:
                   rmdir /s/q C:\Users\<username>\AppData\Local\Microsoft\VisualStudio\11.0\ComponentModelCache
                   
    The builder will get externals via the Tools\buildbot\external*.bat files 
    unless --skip-externals=yes is specified on the command line.
    """

    NASM_URL = 'http://www.nasm.us/pub/nasm/releasebuilds/2.11.03/win32/nasm-2.11.03-win32.zip'

    def __init__(self, config, name):
        
        PythonBuild.__init__(self, config, name)
        
    def _GetSrcDir(self):
        """ Get the source directory; dependencies such as openssl need to go 
        in the parent dir of the unpacked source so a seperate parent dir is
        created for each build, so the directory names look like:
            python\Python-2.7.6\Python-2.7.6\        
            python\Python-2.7.6-dbg\Python-2.7.6-dbg\        
            python\Python-3.4.0\Python-3.4.0\
            
        This changed in python 3.5 where the externals go into a subdirectory
        of the Python-x.x.x directory so the directory names have been changed
        to look like:
            python\Python-3.5.0\
            python\Python-3.5.0-dbg\
        """
        
        from_dist = self.GetOption('copy-from-distribution')
        if from_dist is not None:
            return None
        
        local_dirname = utils.StripExt(self.local_dist_name)
        build_dir = self.GetOption('root') + '/python/' + local_dirname

        dbg_suffix = ''
        if self._GetDebugBuild():
            dbg_suffix += '-dbg'

        suffix = self.name[len('pythonX.X'):]
        
        local_dirname += suffix + dbg_suffix
        if self.version < '3.5':
            build_dir = self.GetOption('root') + (r'\python\%s\%s'
                                                  % (local_dirname, local_dirname))
        else:
            build_dir = self.GetOption('root') + r'\python\%s' % (local_dirname, )
            
        return build_dir
        
    def Build(self):
        
        from_dist = self.GetOption('copy-from-distribution')
        if from_dist is not None:
            self._CopyFromDistribution()
            return
        
        self._UnpackSource()
        self._BuildExternals()
        self._Compile()
        self._Install()

    def _Compile(self):
        """ Compile by using the PCBuild\pcbuild.sln file.  Note that x64
        build for Python 2.5.x doesn't work right now. """
        
        if self._UseBuildBat():
            self._Compile35()
            return
        build_dir = self._GetSrcDir()
        
        pcbuild_sln = build_dir + r'\PCBuild\pcbuild.sln'
      
        vs_version = self._GetVSVersion()
        devenv_com = self._GetDevEnvCom()
            
        nasm_bin_dir = self._GetNasm()
        if nasm_bin_dir is None:
            env = None
        else:
            env = dict(os.environ)
            env['PATH'] += ';' + nasm_bin_dir
        if self._GetVSVersion() > self._GetDefaultVSVersion():
            self._RunDevSolution(['/upgrade'], env)
            print "If dialog appears, click yes to overwrite all project files"
            print "If upgrade fails, look at upgrade log file and if the error is"
            print "  No exports were found that match the constraint: ContractName Microsoft.VisualStudio.Project.IProjectServiceAccessor"
            print "consider doing"
            print "  rmdir /s/q C:\Users\<username>\AppData\Local\Microsoft\VisualStudio\11.0\ComponentModelCache"
        
        x64 = '-x64' in self.name
        
        if not x64:
            if vs_version == 7:
                release_target = 'Release'
                debug_target = 'Debug'
            else:
                release_target = 'Release|Win32'
                debug_target = 'Debug|Win32'
            if self._GetDebugBuild():
                self._RunDevSolution(['/build', debug_target], env)
            else:
                self._RunDevSolution(['/build', release_target], env)
                # _ssl module is only built the 2nd time w/ 2.7.6 release
                self._RunDevSolution(['/build', release_target], env)
        else:
            if vs_version == 7:
                # x64 build using platform SDK compiler, based on
                # http://mail.python.org/pipermail/distutils-sig/2007-May/007650.html
                # Need to build make_versioninfo & make_buildinfo targets in native mode
                # and then set up for x64 and build ReleaseAMD64
                # Note that there's no target for debug x64 in python 2.5.1, but it might
                # be relatively easy for someone to modify one of the existing targets
                # Also attempts to use cmd /c sdk-setup-cmd && build-cmd failed because
                # of win32's broken handling " on command lines and spaces in names.  If
                # this had worked, the temp .bat file wouldn't be needed
                raise NotImplementedError()
            else:
                if self._GetDebugBuild():
                    target = 'Debug'
                else:
                    target = 'Release'
                self._RunDevSolution('/build Release /project make_versioninfo', env=env)
                self._RunDevSolution('/build Release /project make_buildinfo', env=env)
                self._RunDevSolution(['/build', '%s|x64' % target], env=env)
                                
    def _Compile35(self, target=None):
        """ Compile using .bat file introduced in Python 3.5 """
        
        build_dir = self._GetSrcDir()
        pcbuild_dir =  build_dir + r'\PCBuild'
        build_bat = pcbuild_dir + r'\build.bat'
        
        vs_name = self.GetOption('vs-name', default='vs2008')
        
        plat = ('x64' if self._Getx64Build() else 'Win32')
        cmdv = [build_bat, '-e', '-p', plat]
        
        if self._GetDebugBuild():
            cmdv.append('-d')

        if target is not None:
            cmdv += ['-t', target]
            
        # Need to run with vcvars set up in vs2017
        if vs_name is not None and vs_name >= 'vs2017':
            self._RunArgvInEnv(cmdv, pcbuild_dir)
        else:
            utils.RunArgv(cmdv, pcbuild_dir)
        
    def _UseBuildBat(self):
        """ Use the build.bat batch file that was introduced in python 3.5 and
        then backported to 2.7.11. """
        
        if not (self.version == '2.7' or self.version >= '3.5'):
            return False
        
        build_dir = self._GetSrcDir()
        pcbuild_dir =  build_dir + r'\PCBuild'
        build_bat = pcbuild_dir + r'\build.bat'
        return os.path.exists(build_bat)        
            
    def _GetVSVersion(self):
        """ Get the vs version to compile with.  7 = VS 2003, 9 = VS 2008,
        10 = VS 2010 """
        
        if '-vs2010' in self.name:
            vs_version = 10
        elif '-vs2012' in self.name:
            vs_version = 11
        elif '-vs2013' in self.name:
            vs_version = 12
        else:
            vs_version = self._GetDefaultVSVersion()
            
        return vs_version
    
    def _GetDefaultVSVersion(self):
        """ Get the default VS version for the python version """

        if self.version < '2.6':
            vs_version = 7
        elif self.version < '3.3':
            vs_version = 9
        elif self.version < '3.5':
            vs_version = 10
        else:
            vs_version = 14            
            
        return vs_version

    def _GetDevEnvCom(self):
        """ Get the full path name of the devenv.com builder that is used
        to build a solution from the command line. """
        
        # 2.7 with backported build.bat uses devenv from VS 2010 aka VS10
        if self.version == '2.7' and self._UseBuildBat():
            return Win32Config.VS10_DEVENV_COM

        vs_version = self._GetVSVersion()
      
        if vs_version == 7:
            devenv_com = Win32Config.VS7_DEVENV_COM
        elif vs_version == 9:
            devenv_com = Win32Config.VS9_DEVENV_COM
        elif vs_version == 10:
            devenv_com = Win32Config.VS10_DEVENV_COM
        elif vs_version == 11:
            devenv_com = Win32Config.VS11_DEVENV_COM
        elif vs_version == 12:
            devenv_com = Win32Config.VS12_DEVENV_COM
        elif vs_version == 14:
            devenv_com = Win32Config.VS14_DEVENV_COM
        return devenv_com
    
    def _Getx64Build(self):
        
        return '-x64' in self.name
    
    def Clean(self):

        build_dir = self._GetSrcDir()
        if build_dir is None or not os.path.exists(build_dir):
            return

        if self._UseBuildBat():
            self._Compile35(target='CleanAll')
            return
        
        self._RunDevSolution(['/clean'])
            
    def _RunDevSolution(self, args, env=None):
        """ Run devenv.com with the given arguments """
        
        if isinstance(args, str):
            args = args.split(' ')

        build_dir = self._GetSrcDir()
        pcbuild_sln = build_dir + r'\PCBuild\pcbuild.sln'
        devenv_com = self._GetDevEnvCom()

        cmdv = [devenv_com, pcbuild_sln] + list(args)
        utils.RunArgv(cmdv, build_dir, env=env)

    def _BuildExternals(self, force=True):
        """ Build / download externals.  Works aroung bug w/ openssl sources
        by using svn checkout rather than the default svn export """
        
        if self.GetOption('skip-externals') == 'yes':
            return
        
        build_dir = self._GetSrcDir()
        parent_dir = os.path.dirname(build_dir)
        x64 = '-x64' in self.name
        
        # Skip if there's a non-Python directory in build tree
        if not force:
            if self.version >= '3.5':
                if os.path.isdir(build_dir + r'\externals'):
                    return
                
            for name in os.listdir(parent_dir):
                if not name.startswith('Python-'):
                    return
    
        pcbuild_external_bat = build_dir + r'\PCBuild\get_externals.bat'
        if self._UseBuildBat() and os.path.exists(pcbuild_external_bat):
            pcbuild = build_dir + r'\PCBuild'
            utils.RunArgv(pcbuild_external_bat, pcbuild)
            return

        if not x64:
            external_bat = build_dir + r'\Tools\buildbot\external.bat'
        else:
            external_bat = build_dir + r'\Tools\buildbot\external-amd64.bat'
            
        # Work around bug w/ svn export and openssl by using svn checkout instead
        if len(glob.glob(parent_dir + r'\*openssl*')) == 0:
            external_common_bat = build_dir + r'\Tools\buildbot\external-common.bat'
            f = open(external_common_bat, 'r')
            lines = f.readlines()
            f.close()
            lines = [line.strip() for line in lines
                     if 'svn export' in line and 'openssl' in line]
            if len(lines) == 1:
                export_cmd = lines[0][lines[0].find('svn export'):]
                checkout_cmd = export_cmd.replace('export', 'checkout')
                utils.RunArgv(checkout_cmd, parent_dir)

        # Note that running from the build_dir / source dir is important
        utils.RunArgv(external_bat, build_dir)
        
    def _Install(self):
        """ Copy files into the subdirectories in the dist directory 
        (e.g. c:\PythonXX with the standard installer).  This seems to be done
        by Tools\msi\msi.py when the installer is built, but we don't want
        to create a msi installer so it's done by the code below.  Note
        that some files are simply copied from the msi because they either
        are generated when msi.py runs or are compiled with profile 
        guided optimization. """
        
        build_dir = self._GetSrcDir()
        runtime_dir = self._GetRuntimeDir()
        dbg_suffix = ('_d' if self._GetDebugBuild() else '')
        x64 = '-x64' in self.name
        vs_output_dir = build_dir + r'\PCBuild'
        if x64:
            vs_output_dir += r'\amd64'
        elif self.version >= '3.5':
            vs_output_dir += r'\win32'
        py_dll = 'python%s%s%s.dll' % (self.version[0], self.version[2], dbg_suffix)
        
        exe_names = ('python%s.exe' % dbg_suffix, 'pythonw%s.exe' % dbg_suffix)
        for_dlls = [build_dir + r'\PC\py.ico', build_dir + r'\PC\pyc.ico']
        for_libs = []
        for_root = []
        
        name_list = os.listdir(vs_output_dir)
        for name in name_list:
            full_name = os.path.join(vs_output_dir, name)
            if name in exe_names or name == py_dll or name in ['w9xpopen.exe']:
                for_root.append(full_name)
            elif full_name.endswith((dbg_suffix+'.dll', dbg_suffix+'.pyd')):
                for_dlls.append(full_name)
            elif full_name.endswith(dbg_suffix+'.lib'):
                for_libs.append(full_name)
                
        if self._GetDebugBuild():
            no_ext_for_root = set(n[:-4] for n in for_root if n.endswith('.exe'))
            no_ext_for_dll = set(n[:-4] for n in for_root 
                                 if n.endswith(('.dll', '.pyd')))
            for name in [n for n in name_list if n.endswith('.pdb')]:
                full_name = os.path.join(build_dir, name)
                if name[:-4] in no_ext_for_dll:
                    for_dlls.append(full_name)
                elif name[:-4] in no_ext_for_root:
                    for_root.append(full_name)
        
        to_install = {
            runtime_dir: for_root,
            runtime_dir + r'\Lib': [build_dir + r'\Lib'],
            runtime_dir + r'\DLLs': for_dlls,
            runtime_dir + r'\include': [build_dir + r'\Include', build_dir + r'\PC\pyconfig.h'],
            runtime_dir + r'\libs': for_libs,
        }
        for dest_dir, src_list in sorted(to_install.items()):
            if not os.path.exists(dest_dir):
                os.makedirs(dest_dir, 0755)
            for src in src_list:
                utils.Install(src, dest_dir)
                
        for name in ['README', 'LICENSE', 'README.rst']:
            src = os.path.join(build_dir, name)
            dst = os.path.join(runtime_dir, name + '.txt')
            if (os.path.exists(src)
                and (not os.path.exists(dst)
                     or os.stat(src).st_mtime > os.stat(dst).st_mtime)):
                # Copy in text mode to get \r\n newlines
                f = open(src, 'r')
                text = f.read()
                f.close()
                
                f = open(dst, 'w')
                f.write(text)
                f.close()
                
        #self._CopyFromMsi()
        
        if self.GetOption('include-pip') == 'yes':
            python_exe = os.path.join(runtime_dir, 'python{}.exe'.format(dbg_suffix))
            cmdv = [python_exe, '-m', 'ensurepip']
            utils.RunArgv(cmdv, runtime_dir)
        
      
    def _CopyFromMsi(self):
        """ Copy files from .msi for builds w/ default options.  This
        this done since the python.org binaries are built using profile guided
        optimization, which should make a difference. 

        Note: currently not used because PGO optimization is not done in
        python 2.7, which is where this would make a difference to Wingware.
        Also, require some modification for 3.5+.
        """
        
        def get_msi_url():
            for suffix in ['.tar.bz2', '.tgz']:
                if self.url.endswith(suffix):
                    if not self._Getx64Build():
                        url = self.url.replace(suffix, '.msi')
                    else:
                        url = self.url.replace(suffix, '.amd64.msi')
                    # Last part seems to be lower cased
                    parts = url.split('/')
                    parts[-1] = parts[-1].lower()
                    return '/'.join(parts)
                
            raise NotImplementedError()
        
        if self._GetDebugBuild() or self._GetVSVersion() != self._GetDefaultVSVersion():
            return
        
        msi_url = get_msi_url()
        msi_local_file = os.path.join(self.GetOption('root'), 'python',
                                      msi_url.split('/')[-1])
        if not os.path.exists(msi_local_file):
            retr = utils.RetrievalOperations(self.config)
            try:
                retr.Download(msi_url, msi_local_file)
            except Exception:
                print 'Exception occurred when trying to download msi: %r' % msi_url
                return
            
        expanded_dir = os.path.dirname(self._GetSrcDir()) + r'\files-from-msi'
        if not os.path.exists(expanded_dir):
            msiexec_cmd = ['msiexec', '/a', msi_local_file, '/qb', 
                           'TARGETDIR=%s' % expanded_dir]
            self._RunArgvInEnv(msiexec_cmd, os.path.dirname(self._GetSrcDir()))

        runtime_dir = self._GetRuntimeDir()
        
        # Copy LICENSE.txt file since it's pieced together when the msi is built
        utils.Install(expanded_dir + r'\LICENSE.txt', runtime_dir)
        utils.Install(expanded_dir + r'\NEWS.txt', runtime_dir)
        
        if self._GetDebugBuild():
            return
        
        for dirpath, subdirs, files in os.walk(expanded_dir):
            binary_exts = ('.exe', '.dll', '.pyd')
            for name in [f for f in files if f.endswith(binary_exts)]:
                if dirpath == expanded_dir:
                    relative = name
                else:
                    relative = dirpath[len(expanded_dir)+1:] + '\\' + name

                dest = os.path.join(runtime_dir, relative)
                if os.path.exists(dest):
                    
                    src = os.path.join(dirpath, name)
                    utils.Install(src, dest)
                        
    def _GetNasm(self):
        """ Get a copy of nasm (if needed) and return the full name of the 
        directory it's unpacked in.  nasm is an assembler that is needed to
        compile openssl and needs to be on the PATH when the pcbuild build
        process runs. """

        if self.GetOption('skip-externals') == 'yes':
            return None
        
        build_dir = self._GetSrcDir()
        parent_dir = os.path.dirname(build_dir)

        nasm_dir = os.path.join(parent_dir, 'nasm')
        if not os.path.exists(nasm_dir):
            os.mkdir(nasm_dir)
            
        for name in os.listdir(nasm_dir):
            if name.startswith('nasm-') and os.path.isdir(os.path.join(nasm_dir, name)):
                return os.path.join(nasm_dir, name)
            
        nasm_zip = os.path.join(nasm_dir, self.NASM_URL.split('/')[-1])
        retr = utils.RetrievalOperations(self.config)
        retr.Download(self.NASM_URL, nasm_zip)
        
        z = zipfile.ZipFile(nasm_zip)
        z.extractall(nasm_dir)

        for name in os.listdir(nasm_dir):
            full_name = os.path.join(nasm_dir, name)
            if name.startswith('nasm-') and os.path.isdir(full_name):
                utils.Install(full_name + r'\nasm.exe', full_name + r'\nasmw.exe')
                return full_name
            
        return None
        
if sys.platform == 'win32':   
    projbuilder.RuntimeBuilder._RegisterSubClass(Win32PythonBuild)
else:
    projbuilder.RuntimeBuilder._RegisterSubClass(PythonBuild)

class StacklessBuild(PythonBuild):
    
    prefix = 'stackless'

    SRC_URLS = {
        '2.7': 'http://www.stackless.com/binaries/stackless-275-export.tar.bz2',
        }

    def _GetTargetPython(self):
        
        info = super(StacklessBuild, self)._GetTargetPython()
        info.runtime_suffix = 'stackless' + info.version
        info.proj_name = self.name
        return info

projbuilder.RuntimeBuilder._RegisterSubClass(StacklessBuild)
