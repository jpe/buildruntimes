# Copyright (c) 2001-2012, Archaeopteryx Software, Inc.  All rights reserved.
#
# Written by John P. Ehresman and Stephan R.A. Deibel
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from buildruntimes import utils
import sys
import os
import ConfigParser as configparser

def GetPlatformIdentifier():
    """ Return platform identifier -- 'osx', 'win32', 'linux-x32', or 'linux-x64' """
    
    if sys.maxint > (1 << 32):
        arch = 'x64'
    else:
        arch = 'x32'

    if sys.platform == 'darwin':
        return 'osx'
    elif sys.platform == 'win32':
        return 'win32'
    elif sys.platform.startswith('linux'):
        return 'linux-' + arch
    else:
        return sys.platform + '-' + arch

class _VisualStudioInfo:

    VERSION_FOR_NAME = {
        'vs2008': 9,
        'vs2010': 10,
        'vs2012': 11,
        'vs2013': 12,
        'vs2015': 14,
        }
    
    def __init__(self, vs_name):
        
        if vs_name == 'vs2003':
            pf_name = "Microsoft Visual Studio .NET 2003"
            bat_name = r"VC7\vcvars32.bat"
        elif vs_name == 'vs2017':
            pf_name = "Microsoft Visual Studio"
            bat_name = r'2017\Professional\VC\Auxiliary\Build\vcvars32.bat'
        else:
            pf_name = ("Microsoft Visual Studio %s.0" 
                       % (str(self.VERSION_FOR_NAME[vs_name]),))
            bat_name = r'VC\vcvarsall.bat'
            
        # Attribs are None if vs isn't installed or sys.platform != 'win32'
        self.vs_name = vs_name
        self.vs_dir = None
        self.vcvars_bat = None
        self.devenv_exe = None
        
        if sys.platform != 'win32':
            return
        
        self.vs_dir = utils.InProgramFiles(pf_name)
        if self.vs_dir is None:
            return
        
        self.vcvars_bat = os.path.join(self.vs_dir, bat_name)
        self.devenv_exe = os.path.join(self.vs_dir, r'Common7\IDE\devenv.exe')

class Config:
    """ Configuration class that contains file names and other configurable
    settings and provide utility methods that use these settings. """

    BITBUCKET_JPE_GIT_REPOSITORIES = {
        'qt': ('git', 'git://gitorious.org/qt/qt.git', '4.8', 'v4.8.3'),
        'shiboken': ('git', 'https://bitbucket.org/jpe/experimental-shiboken.git', 'wingide'),
        'pyside': ('git', 'https://bitbucket.org/jpe/experimental-pyside.git', 'wingide'),
        'scintillaeditpy': ('hg', 'https://jpe@bitbucket.org/jpe/wing-scintilla', 'wingide'),
    }
    GITORIUS_REPOSITORIES = {
        'qt': ('git', 'git://gitorious.org/qt/qt.git', '4.8', 'v4.8.3'),
        'qt4.5': ('git', 'git://gitorious.org/qt/qt.git', '4.5', 'v4.5.3'),
        'qt4.6': ('git', 'git://gitorious.org/qt/qt.git', '4.6', 'v4.6.5'),
        'qt4.7': ('git', 'git://gitorious.org/qt/qt.git', '4.7', 'v4.7.6'),
        'shiboken': ('git', 'git://gitorious.org/pyside/shiboken.git', 'master'),
        'pyside': ('git', 'git://gitorious.org/pyside/pyside.git', 'master'),
    }
    GIT_REPOSITORIES = GITORIUS_REPOSITORIES

    PERL_BIN_DIR = None

    CMAKE = 'cmake'
    GIT = 'git'
    GIT_SSH = None
    HG = 'hg'
    SCP = 'scp'
    
    PLATFORM = GetPlatformIdentifier()
    
    # These are win32 settings, but it can be set elsewhere
    VC_VCVARS32_BAT_NAMES = {
        7: r"Microsoft Visual Studio .NET 2003\VC7\vcvars32.bat",
        9: r"Microsoft Visual Studio 9.0\VC\vcvarsall.bat",
        10: r"Microsoft Visual Studio 10.0\VC\vcvarsall.bat",
        12: r"Microsoft Visual Studio 12.0\VC\vcvarsall.bat",
    }

    VS_INFO = {
        'vs2008': _VisualStudioInfo('vs2008'),
        'vs2010': _VisualStudioInfo('vs2010'),
        'vs2012': _VisualStudioInfo('vs2012'),
        'vs2013': _VisualStudioInfo('vs2013'),
        'vs2015': _VisualStudioInfo('vs2015'),
        'vs2017': _VisualStudioInfo('vs2017'),
        }

    if sys.platform == 'win32':
        PERL_BIN_DIR = r'c:\Perl\bin'
        CMAKE = utils.InProgramFiles(r'CMake 2.8\bin\cmake.exe')
        GIT = utils.InProgramFiles(r'Git\bin\git.exe')
        GIT_SSH = utils.InProgramFiles(r'Putty\plink.exe')
        SCP = utils.InProgramFiles(r'Putty\pscp.exe')

    def __init__(self, root, args):

        if sys.platform == 'win32':
            target_python = sys.executable
        else:
            target_python = os.path.basename(sys.executable)
            
        self.arg_options = {}
        for a in args:
            equal_pos = a.find('=')
            if equal_pos != -1 and a.startswith('--'):
                name = a[2:equal_pos]
                val = a[equal_pos+1:]
                self.arg_options[name] = val
                
        ini_filename = self.arg_options.get('ini-filename',
                                            'buildruntimes.ini')
        ini_filename = os.path.expanduser(ini_filename)
        root = self.arg_options.get('root', root)
        root = os.path.expanduser(root)
        
        if sys.platform == 'darwin':
            platform = 'osx'
        elif sys.platform.startswith('linux'):
            platform = 'linux'
        else:
            platform = sys.platform
        defaults = {'platform': platform,
                    'target-python': target_python,
                    'debug': 'yes',
                    'root': root,
                    }
        self.ini_file = configparser.ConfigParser(defaults)
        self.ini_file.default_section = 'DEFAULT'
        if os.path.exists(ini_filename):
            self.ini_file.read(ini_filename)

    def GetSourceRepository(self, name):
        """ Returns object with attributes:
              kind (git or hg)
              url
              branch
              tag
         """

        entry = self.GIT_REPOSITORIES.get(name)
        if entry is None:
            return None
        
        info = utils.RetrievalOperations.Info()
        if len(entry) == 2:
            info.kind = 'git'
            info.url = entry[0]
            info.branch = entry[1]
            info.tag = None
        elif len(entry) == 3:
            info.kind = entry[0]
            info.url = entry[1]
            info.branch = entry[2]
            info.tag = None
        elif len(entry) == 4:
            info.kind = entry[0]
            info.url = entry[1]
            info.branch = entry[2]
            info.tag = entry[3]
        return info

    def _GetPythonVersion(self, target_python):

        pyversion = target_python[-3:]
        if not (len(pyversion) == 3 and pyversion[0].isdigit()
                and pyversion[1] == '.' and pyversion[2].isdigit()):
            pyversion = '2.7'

        return pyversion
