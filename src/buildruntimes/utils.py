# Copyright (c) 2001-2012, Archaeopteryx Software, Inc.  All rights reserved.
#
# Written by John P. Ehresman and Stephan R.A. Deibel
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import sys
import shutil
import subprocess
import urllib2
import tarfile

def InProgramFiles(name):
    """ Look for name in either c:\Program Files (x86) or c:\Program Files and
    return first one that exists. """

    possibles = [
        r'c:\Program Files (x86)',
        r'c:\Program Files',
    ]
    for base in possibles:
        dirname = os.path.join(base, name)
        if os.path.exists(dirname):
            return dirname

    return os.path.join(r'c:\Program Files', name)

def Install(src, dst, force=False, filter_func=None):
    """ Install a file or directory to dst.  If dst is an existing directory or 
    ends in a slash and src is a file then the file is copied to 
    dst/basename(src).  If force is false, file is only copied if it's newer 
    than dst.  If filter_func is not None, it is called with the src name
    and the src is installed iff filter_func(src) evaluates to true. """
    
    if not os.path.exists(src):
        print('Cannot install, src does not exist: ' + src)
        return
    
    if filter_func is not None and not filter_func(src):
        print('Not copying because filter_func returned false: ' + src)
        return
    
    # If dst ends with / and doesn't exist, create it
    seps = (('/', '\\') if sys.platform == 'win32' else ('/', ))
    if dst.endswith(seps) and not os.path.exists(dst):
        os.makedirs(dst)

    # src is a directory so copy recursively
    if src.endswith('/') or (os.path.isdir(src) and not os.path.islink(src)):
        if not os.path.exists(dst):
            os.makedirs(dst)
        for name in os.listdir(src):
            Install(os.path.join(src, name), os.path.join(dst, name), force,
                    filter_func=filter_func)
        return

    # Non-recursive: make sure dst is a filename
    if os.path.isdir(dst) or dst.endswith(seps):
        dst = os.path.join(dst, os.path.basename(src))
                
    # a symlink
    if sys.platform != 'win32' and os.path.islink(src):
        linkto = os.readlink(src)
        if not force and os.path.islink(dst) and os.readlink(dst) == linkto:
            return

        if os.path.islink(dst):
            os.remove(dst)
        os.symlink(linkto, dst)
        
    # a file
    else:
        if (not force and os.path.exists(dst)
            and os.stat(dst).st_mtime == os.stat(src).st_mtime
            and os.path.islink(src) == os.path.islink(dst)):
            return
        
        try:
            shutil.copy2(src, dst)
        except IOError:
            os.remove(dst)
            shutil.copy2(src, dst)

def SymLink(target, link_name):
    """ Create a symlink to the target.  Does nothing if it already exists
    and links to target. """
    
    if os.path.islink(link_name) and os.readlink(link_name) == target:
        return
    
    os.symlink(target, link_name)

def RunArgv(argv, rundir, env=None, shell=False, check=False):
    """ Run argv via subprocess in env.  Note that argv can be a string
    on win32 -- it's converted to a string anyway if it's a list which
    can play havoc with win32 bizarre quoting conventions.  If check
    is true, subprocess.check_call is used """

    rundir = os.path.normpath(rundir)
    print 'Running: %s' % (argv if isinstance(argv, basestring)
                           else ' '.join(argv))
    print 'In directory: %s' % rundir
    if check:
        return subprocess.check_call(argv, env=env, cwd=rundir, shell=shell)
    else:
        return subprocess.call(argv, env=env, cwd=rundir, shell=shell)

class RetrievalOperations:
    """ Support for retrieving source from its repository / server
    location """

    class Info:
        kind = None
        url = None
        branch = None
        tag = None
    
    def __init__(self, config):
        
        self.config = config

    def RetrieveIntoDir(self, info, dirname):

        if os.path.exists(dirname):
            raise ValueError("Directory Exists: %s" % dirname)

        if info.kind == 'hg':
            self.HgClone(info.url, dirname, branch=info.branch, tag=info.tag)
        elif info.kind == 'git':
            self.GitClone(info.url, dirname, branch=info.branch, tag=info.tag)
        elif info.kind == 'tarball':
            url = info.url
            if url.startswith('file:') and os.sep != '/':
                url = url.replace(os.sep, '/')
            last_part = url.split('/')[-1]
            local_file = os.path.join(os.path.dirname(dirname), last_part)

            self.Download(url, local_file)
            if local_file.endswith('.zip'):
                UnpackZip(local_file, dirname)
            else:
                UnpackTar(local_file, dirname)
            
            
    def UpdateDir(self, info, dirname):
        """ Update given directory from the repository """

        if not os.path.exists(dirname):
            raise ValueError("Directory Does Not Exist: %s" % dirname)

        if info.kind == 'hg':
            argv = [self.config.HG, 'pull', '-u']
            RunArgv(argv, rundir=dirname, check=True)
        elif info.kind == 'git':
            if info.branch is not None:
                argv = [self.config.GIT, 'checkout', info.branch]
                RunArgv(argv, rundir=dirname, check=True)

            argv = [self.config.GIT, 'pull']
            RunArgv(argv, rundir=dirname, check=True)

            if info.tag is not None:
                argv = [self.config.GIT, 'checkout', info.tag]
                RunArgv(argv, rundir=dirname, check=True)

    def HgClone(self, repo, dirname, branch=None, tag=None):

        if os.path.exists(dirname):
            return

        print 'Cloning %s...' % repo, 'into', dirname

        argv = [self.config.HG, 'clone', repo, os.path.basename(dirname)]
        RunArgv(argv, rundir=os.path.dirname(dirname), check=True)

        if branch is not None:
            argv = [self.config.HG, 'checkout', branch]
            RunArgv(argv, rundir=dirname, check=True)

        if tag is not None:
            argv = [self.config.HG, 'checkout', '-r', tag]
            RunArgv(argv, rundir=dirname, check=True)

    def GitClone(self, repo, dirname, branch=None, tag=None):
        """ Run git clone to create directory with given branch & tag
        checked out """

        if os.path.exists(dirname):
            return

        print 'Cloning %s...' % repo, 'into', dirname

        argv = [self.config.GIT, 'clone', repo, os.path.basename(dirname)]
        if branch is not None:
            argv.extend(['-b', branch])
        RunArgv(argv, rundir=os.path.dirname(dirname), check=True)

        if tag is not None:
            argv = [self.config.GIT, 'checkout', tag]
            RunArgv(argv, rundir=dirname, check=True)

    def DownloadToDir(self, url, dirname, force=False):
        """ Download url to a file in the dirname directory.  If file exists,
        does not download if force isn't true """
        
        if url.startswith('file:') and os.sep != '/':
            url = url.replace(os.sep, '/')
        last_part = url.split('/')[-1]
        local_file = os.path.join(dirname, last_part)

        if not os.path.exists(local_file):
            self.Download(url, local_file)
        
        return local_file
                
    def Download(self, url, dist_name):
        """ Download from url to dist_name file if local file doesn't exist. """

        if os.path.exists(dist_name):
            return

        if url.startswith('file://'):
            local_file = url[len('file://'):]
            print 'Copying local file:', local_file
    
            dists_dir = os.path.dirname(dist_name)
            if not os.path.exists(dists_dir):
                os.makedirs(dists_dir)

            shutil.copyfile(local_file, dist_name)
            return
                             
        print 'Downloading:', url
        print dist_name

        f = urllib2.urlopen(url)
        chunk_size = 50 * 1024
        part_list = []
        part = f.read(chunk_size)
        while part != '':
            part_list.append(part)
            sys.stdout.write('.')
            sys.stdout.flush()
            part = f.read(chunk_size)
        f.close()
        sys.stdout.write('\n')
        data = ''.join(part_list)

        dists_dir = os.path.dirname(dist_name)
        if not os.path.exists(dists_dir):
            os.makedirs(dists_dir)
        f = open(dist_name, 'wb')
        f.write(data)
        f.close()

def StripExt(archive_name):
    """ Strip the extension off of the name """

    for suffix in ['.zip', '.tar.bz2', '.tar.gz', '.tar.xz', '.tgz', '.tar']:
        if archive_name.endswith(suffix):
            archive_name = archive_name[:-len(suffix)]
    return archive_name

def UnpackTar(tar_name, dest_dir):
    """ Unpack named tar file into dest_dir.  If the tar file contains
    a single top level directory, that directory is omitted when unpacking. """

    def get_single_top_dirname(tar_name):
        
        tf = tarfile.open(tar_name)
        try:
            top_dirname = None
            for member in tf:
                parts = member.name.split('/')
                if top_dirname is None:
                    top_dirname = parts[0]
                else:
                    if parts[0] != top_dirname:
                        return None
                    
        finally:
            tf.close()

        return top_dirname
    
    local_name = os.path.basename(tar_name)
    if local_name.endswith('.xz') and sys.platform != 'win32':
        RunArgv(['sh', '-c', 'unxz -c {} > {}'.format(local_name, local_name[:-3])],
                rundir=os.path.dirname(tar_name))
        tar_name = tar_name[:-3]
        local_name =  local_name[:-3]

    top_dirname = get_single_top_dirname(tar_name)
    
    tf = tarfile.open(tar_name)
    tarbasename = StripExt(local_name)
    for member in tf:
        if top_dirname is not None:
            name = member.name[len(top_dirname) + 1:]
        else:
            name = member.name
        if not os.path.isabs(name):
            dest = os.path.join(dest_dir, name)
            tf._extract_member(member, dest)
    tf.close()

def UnpackZip(zip_name, dest_dir):
    """ Unpack named zip file into dest_dir.  If the zip file contains
    a single top level directory, that directory is omitted when unpacking. """

    import zipfile

    def get_single_top_dirname(zip_name):
        
        zf = zipfile.ZipFile(zip_name)
        try:
            top_dirname = None
            for info in zf.infolist():
                filename = info.filename.replace('\\', '/')
                parts = filename.split('/')
                if top_dirname is None:
                    top_dirname = parts[0]
                else:
                    if parts[0] != top_dirname:
                        return None
                    
        finally:
            zf.close()

        return top_dirname

    top_dirname = get_single_top_dirname(zip_name)

    zf = zipfile.ZipFile(zip_name)
    for info in zf.infolist():
        filename = info.filename.replace('\\', '/')
        if filename.endswith('/'):
            continue
        
        if top_dirname is not None:
            name = filename[len(top_dirname) + 1:]
        else:
            name = filename
        
        if not os.path.isabs(name):
            dest = os.path.join(dest_dir, name)
            member_f = zf.open(info, 'r')
            data = member_f.read()
            member_f.close()
            
            if not os.path.exists(os.path.dirname(dest)):
                os.makedirs(os.path.dirname(dest))

            dest_f = open(dest, 'wb')
            dest_f.write(data)
            dest_f.close()
            
    zf.close()

def IsScpRemoteName(name):
    """ Returns whether the given name looks like a remote name for scp """

    # : after a drive letter could be a local name
    pos = name.find(':')
    if pos > 1:
        return True
    else:
        return False
