# Copyright (c) 2001-2012, Archaeopteryx Software, Inc.  All rights reserved.
#
# Written by John P. Ehresman and Stephan R.A. Deibel
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from buildruntimes.config import Config
from buildruntimes import projbuilder
from buildruntimes import utils

import glob
import os
import shutil
import subprocess
import sys


class Commands:
    """ Build or packaging commands that are exposed via command line arguments.
    Any lower cased method name can be invoked via the command line -- e.g. 
    "buildruntimes.py build" invokes the build method. """


    default_config = None

    def __init__(self, config=None):
        self.config = (config if config is not None else
                       self.default_config)
        assert self.config is not None

    def _ExtractOptions(self, args, name_prefixes=()):
        """ Go through sequence of args and for any that starts with a <name>=
        for any name in name_prefixes, put its value in an options dictionary.
        Returns 2 item tuple of list of remaining args and dict of found options """

        pos_args = []
        options = {}
        for a in args:
            handled = False
            if a.startswith('--'):
                if '=' in a:
                    name = a[2:a.find('=')]
                    options[name] = a[a.find('=') + 1:]
                else:
                    name = a[2:]
                    options[name] = True
                handled = True

            if not handled:
                pos_args.append(a)

        return pos_args, options

    def upload(self, *args):
        """ Upload runtime archive.
        Usage: upload <project>+ --remote-dir=<scp remote directory>
               [--include-debugfiles=yes] """

        project_list, options = self._ExtractOptions(args, ['--remote-dir'])

        for name in project_list:
            builder = projbuilder.GetBuilderForName(name, self.config)
            remote_dir = builder.GetOption('remote-dir', options)

            archive_name_list = [builder._GetRuntimeDir() + '.tar.bz2']
            if builder.GetOption('include-debugfiles', options) == 'yes':
                archive_name_list.append(
                    builder._GetRuntimeDir() + '-debugfiles.tar.bz2')
            
            for archive_name in archive_name_list:
                if utils.IsScpRemoteName(remote_dir):
                    argv = [self.config.SCP, archive_name, remote_dir]
                    utils.RunArgv(argv, os.path.dirname(archive_name))
                else:
                    shutil.copy(archive_name, remote_dir)

    def download(self, *args):
        """ Download runtime archive.
        Usage: download <project>+ --remote-dir=<scp remote directory>
               [--include-debugfiles=yes] """

        project_list, options = self._ExtractOptions(args, ['--remote-dir'])

        for name in project_list:
            builder = projbuilder.GetBuilderForName(name, self.config)
            remote_dir = builder.GetOption('remote-dir', options)

            archive_name_list = [builder._GetRuntimeDir() + '.tar.bz2']
            if builder.GetOption('include-debugfiles', options) == 'yes':
                archive_name_list.append(
                    builder._GetRuntimeDir() + '-debugfiles.tar.bz2')
            
            for archive_name in archive_name_list:
                base_name = os.path.basename(archive_name)
                if utils.IsScpRemoteName(remote_dir):
                    argv = [self.config.SCP, remote_dir + '/' + base_name, archive_name]
                    utils.RunArgv(argv, os.path.dirname(archive_name))
                else:
                    shutil.copy(os.path.join(remote_dir, base_name), archive_name)

    def archive(self, *args):
        """ Archive runtime files.
        Usage: archive <project>+ """

        project_list, options = self._ExtractOptions(args)

        for project in project_list:
            builder = projbuilder.GetBuilderForName(project, self.config)
            builder.Archive()
            if builder.GetOption('include-debugfiles', options) == 'yes':
                builder.ArchiveDebugFiles()

    def expand(self, *args):
        """ Expand runtime files.
        Usage: expand <project>+ """

        project_list, options = self._ExtractOptions(args)

        for project in project_list:
            builder = projbuilder.GetBuilderForName(project, self.config)
            exists = os.path.exists(builder._GetRuntimeDir(base_ok=True))
            if not exists or builder.GetOption('force', options) == 'yes':
                builder.Expand()
            if builder.GetOption('include-debugfiles', options) == 'yes':
                builder.ExpandDebugFiles()

    def archivedebugfiles(self, *args):
        """ Archive source and intermediate files needed for debugging.
        Usage: archivedebugfiles <project>+ """

        project_list, options = self._ExtractOptions(args)

        for project in project_list:
            builder = projbuilder.GetBuilderForName(project, self.config)

    def expanddebugfiles(self, *args):
        """ Expand source and intermediate files needed for debugging.
        Usage: expanddebugfiles <project>+ """

        project_list, options = self._ExtractOptions(args)

        for project in project_list:
            builder = projbuilder.GetBuilderForName(project, self.config)
            builder.ExpandDebugFiles()

    def shell(self, *args):
        """ Run shell with the env for the runtime environment set.
        Usage: shell [<project>] """

        cmdv = [('cmd.exe' if sys.platform == 'win32' else 'bash')]

        project_list, options = self._ExtractOptions(args)
        
        name = 'pyside'
        if len(project_list) != 0:
            name = project_list[0]
        if len(project_list) > 1:
            cmdv = project_list[1:]
            
        builder = projbuilder.GetBuilderForName(name, self.config)
        if name in ('pyside', 'shiboken'):
            run_dir = builder._GetBuildDir()
        else:
            run_dir = builder._GetSrcDir()
            
        builder._RunArgvInEnv(cmdv, run_dir, include_self=True)

    def build(self, *args):
        """ Build projects
        Usage: build <project>+ """

        project_list, options = self._ExtractOptions(args)
        if 'qt' in project_list and len(project_list) != 1:
            print 'qt must be built alone'
            return -1

        for name in project_list:
            builder = projbuilder.GetBuilderForName(name, self.config)
            if builder is None:
                print('Unknown project: %s' % name)
            else:
                builder.Build()

    def clean(self, *args):
        """ Clean projects
        Usage: clean <project>+ """

        project_list, options = self._ExtractOptions(args)
        if 'qt' in project_list and len(project_list) != 1:
            print 'qt must be cleaned alone'
            return -1

        for name in project_list:
            builder = projbuilder.GetBuilderForName(name, self.config)
            if builder is None:
                print('Unknown project: %s' % name)
            else:
                builder.Clean()

    def removeruntime(self, *args):
        """ Remove runtime directories and archive files
        Usage: removeruntime <project>+ """

        project_list, options = self._ExtractOptions(args)

        for name in project_list:
            builder = projbuilder.GetBuilderForName(name, self.config)
            runtime_dir = builder._GetRuntimeDir()
            print runtime_dir
            if os.path.isdir(runtime_dir):
                shutil.rmtree(runtime_dir)
            for name in glob.glob(runtime_dir + '*.tar.bz2'):
                os.remove(name)
            for name in glob.glob(runtime_dir + '*.tar.bz2.runtime-info'):
                os.remove(name)
                
    def script(self, *args):
        
        for single_arg in args:
            if not single_arg.startswith('--') and single_arg.endswith('.py'):
                gl_dict = {'__file__': single_arg}
                execfile(single_arg, gl_dict, gl_dict)

    def updatesource(self, *args):
        """ Build projects
        Usage: build <project>+ """

        project_list, options = self._ExtractOptions(args)
        for name in project_list:
            builder = projbuilder.GetBuilderForName(name, self.config)
            if builder is None:
                print('Unknown project: %s' % name)
            else:
                builder.UpdateSource()
                

def ParseArgs(argv, root):

    default_root = root
    class Options:
        root = default_root
        debug = 'yes'
        target_python = 'python2.7'
        cmd = None
        projects = ()

        def __init__(self):
            self.projects = []

    options = Options()

    i = 0
    while i < len(argv):
        a = argv[i]
        i += 1

        if a.startswith('--root='):
            options.root = a[len('--root='):]
        elif options.cmd is None and not a.startswith('--'):
            options.cmd = a
        else:
            options.projects.append(a)

    return options




def Main(argv, root=None):
    
    try:
        if root is None:
            root = os.path.dirname(os.path.dirname(os.path.abspath(argv[0])))
    
        options = ParseArgs(argv[1:], root)
    
        config = Config(options.root,
                        [a for a in options.projects if a.startswith('--')])
        Commands.default_config = config
      
        if callable(getattr(Commands, options.cmd)):
            cmds = Commands(config)
            method = getattr(cmds, options.cmd)
            method(*options.projects)
        else:
            print 'Unknown command:', options.cmd
    
    except subprocess.CalledProcessError, exc:
        print(exc)
        return exc.returncode


