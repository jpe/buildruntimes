# Copyright (c) 2001-2012, Archaeopteryx Software, Inc.  All rights reserved.
#
# Written by John P. Ehresman and Stephan R.A. Deibel
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import glob
import os
import sys
import shutil
import tarfile
try:
    import multiprocessing
except ImportError:
    multiprocessing = None

from buildruntimes import projbuilder
from buildruntimes import utils


class _CMakeBasedProject(projbuilder.RuntimeBuilder):
    """ Common base class for both shiboken and pyside, which are built
    in the same manner.  Both are built via cmake and the build directory
    is the build/debug-pythonX.X or build/release-pythonX.X (so there
    can be both debug and release builds for multiple python versions) """

    _separate_debug_runtime_dir = True
    _separate_pyversion_runtime_dir = True
    
    def __init__(self, config, name, git_url):

        projbuilder.RuntimeBuilder.__init__(self, config)

        self.name = name
        # Note that pyside is a dependency because shiboken is installed into it
        # and is needed at pyside build time
        target_python = self._GetTargetPython().proj_name
        target_qt = self.GetOption('target-qt', default='qt')
        self._dependencies = ['cmake', target_python, 'pyside']
        if target_qt != 'system-qt':
            self._dependencies.append(target_qt)
        self._runtime_dir_suffixes = [self._GetTargetPython().runtime_suffix]
        if target_qt not in ('qt', 'system-qt'):
            self._runtime_dir_suffixes.append(target_qt)
        self.git_origin = git_url

    def _GetRuntimeDir(self, base_ok=False):

        if self.name == 'pyside':
            return projbuilder.RuntimeBuilder._GetRuntimeDir(self)
        else:
            pyside_builder = projbuilder.GetBuilderForName('pyside', self.config)
            return pyside_builder._GetRuntimeDir(base_ok=base_ok)

    def _GetSrcDir(self):

        print self.name
        return self.GetOption('root') + '/' + self.name

    def _GitClone(self):

        src_dir = self._GetSrcDir()
        if os.path.exists(src_dir):
            return

        info = self.config.GetSourceRepository(self.name)
        info.url = self.GetOption('origin-url', default=info.url)
        info.branch = self.GetOption('origin-branch', default=info.branch)
        retr = utils.RetrievalOperations(self.config)
        retr.RetrieveIntoDir(info, src_dir)

    def Build(self):

        self._GitClone()
        self._EnsureBuildDirExists()
        self._RunCmake()
        self._RunMake()

    def Clean(self):

        build_dir = self._GetSrcDir() + '/build'
        if os.path.isdir(build_dir):
            shutil.rmtree(build_dir)

    def _EnsureBuildDirExists(self):

        full_build_dir = self._GetBuildDir()
        if not os.path.exists(full_build_dir):
            print 'Creating %s build dir' % self.name
            os.makedirs(full_build_dir)

    def _RunCmake(self):

        full_build_dir = self._GetBuildDir()

        print 'Running cmake for %s' % self.name
        pyside_runtime = self._GetRuntimeDir()
        target_python = self._GetTargetPython()
        env_py_prefix = ('PYTHON' if target_python.version < '3.0' else 'PYTHON3')

        argv = [self.config.CMAKE, '../..',
                '-DCMAKE_INSTALL_PREFIX=%s' % pyside_runtime,
                '-DENABLE_ICECC=0']
        py_builder = projbuilder.GetBuilderForName(target_python.proj_name,
                                                   self.config)
        py_runtime = py_builder._GetRuntimeDir(base_ok=True)
        runtime_list = [py_runtime, pyside_runtime]
        argv.append('-DCMAKE_PREFIX_PATH=%s' % ':'.join(runtime_list))
        if sys.platform == 'win32':
            argv.append('-GNMake Makefiles')
        elif sys.platform == 'darwin':
            # pyside CMakelists claims this is to work around a cmake 2.8 bug
            qt_builder = projbuilder.GetBuilderForName('qt', self.config)
            qt_runtime = qt_builder._GetRuntimeDir(base_ok=True)
            argv.append('-DALTERNATIVE_QT_INCLUDE_DIR=%s/include' % qt_runtime)
            lib_dir = os.path.normpath(self._GetTargetPython().executable + '/../../lib')
            framework_dir = lib_dir + '/Python.framework'
            argv.append('-DPython_FRAMEWORKS=%s' % framework_dir)

            argv.append('-D%s_LIBRARY=%s/Versions/%s/Python'
                        % (env_py_prefix, framework_dir,
                           self._GetTargetPython().version))
            argv.append('-D%s_INCLUDE_DIR=%s/Headers'
                        % (env_py_prefix, framework_dir))
        else: # Linux and other unix-like
            argv.append('-D%s_INCLUDE_DIR=%s/include/python%s'
                        % (env_py_prefix, py_runtime, target_python.version))
            argv.append('-D%s_LIBRARY=' % env_py_prefix)
            argv.append('-D%sLIBS_FOUND=True' % env_py_prefix)

        if sys.platform != 'win32':
            py_executable = target_python.executable
            argv.append('-D%s_EXECUTABLE=%s' % (env_py_prefix, py_executable))
            if self._GetDebugBuild():
                argv.append('-D%s_DBG_EXECUTABLE=%s' % (env_py_prefix, py_executable))
        else:
            pybin_dir = os.path.dirname(target_python.executable)
            py_import_lib = os.path.join(
                pybin_dir, 'libs', 'python%s%s.lib' % (target_python.version[0],
                                                       target_python.version[2]))
            if not self._GetDebugBuild():
                argv.extend([
                    #'-DCMAKE_DEBUG_POSTFIX=_d',
                    r'-DPYTHON_EXECUTABLE=%s' % target_python.executable,
                    r'-DPYTHON_LIBRARY=%s' % py_import_lib,
                    #r'-DPYTHON_DEBUG_LIBRARY=%s\python%s.lib'%(pyclib_dir, py2digit_version),
                ])
            else:
                py_import_lib = py_import_lib[:-len('.lib')] + '_d.lib'
                argv.extend([
                    '-DCMAKE_DEBUG_POSTFIX=_d',
                    r'-DPYTHON_EXECUTABLE=%s' % target_python.executable,
                    r'-DPYTHON_LIBRARY=%s' % py_import_lib,
                    r'-DPYTHON_DEBUG_LIBRARY=%s' % py_import_lib,
                ])

        if self._GetDebugBuild():
            argv.append('-DCMAKE_BUILD_TYPE=Debug')
        else:
            argv.append('-DCMAKE_BUILD_TYPE=Release')

        if target_python.version.startswith('3.'):
            argv.append('-DUSE_PYTHON3=True')

        self._RunArgvInEnv(argv, rundir=full_build_dir)

    def _GetBuildDir(self):
        src_dir = self._GetSrcDir()
        if self._GetDebugBuild():
            full_build_dir = os.path.join(src_dir, 'build', 'debug')
        else:
            full_build_dir = os.path.join(src_dir, 'build', 'release')

        python_suffix = self._GetTargetPython().runtime_suffix
        full_build_dir += '-' + python_suffix
        return full_build_dir

    def _RunMake(self):

        full_build_dir = self._GetBuildDir()
        
        print 'Running make for %s' % self.name
        if sys.platform == 'win32':
            argv = ['nmake', 'install']
        else:
            make_j_count = 2
            if multiprocessing is not None:
                try:
                    make_j_count = multiprocessing.cpu_count()
                except NotImplementedError:
                    pass

            argv = ['make', 'install', '-j', str(make_j_count)]
            
        self._RunArgvInEnv(argv, rundir=full_build_dir)

        self._PostInstall()

    def _PostInstall(self):

        # Method for subclasses to possibly override
        pass
    


class PysideBuild(_CMakeBasedProject):
    
    name = 'pyside'

    def __init__(self, config):

        git_origin = config.GIT_REPOSITORIES['pyside']
        _CMakeBasedProject.__init__(self, config, 'pyside', git_origin)

    def _PostInstall(self):

        build_dir = self._GetBuildDir()
        runtime_dir = self._GetRuntimeDir()
        if sys.platform == 'win32':
            py_dst_dir = os.path.join(runtime_dir, 'lib', 'site-packages',
                                      'PySide')
            assert os.path.isdir(py_dst_dir)

            for name in glob.glob(build_dir + r'\PySide\*.pdb'):
                utils.Install(name, py_dst_dir)
            for name in glob.glob(build_dir + r'\libpyside\*.pdb'):
                utils.Install(name, runtime_dir + r'\lib')

    def ArchiveDebugFiles(self, archive_name=None):

        shiboken_builder = projbuilder.GetBuilderForName('shiboken', self.config)

        src_dir = self._GetSrcDir()
        runtime_dir = self._GetRuntimeDir()
        if archive_name is None:
            archive_name = runtime_dir + '-debugfiles.tar.bz2'

        tar_f = tarfile.open(archive_name, 'w:bz2')

        for builder in [self, shiboken_builder]:
            src_dir = builder._GetSrcDir()
            filtered_names = [name for name in os.listdir(src_dir)
                              if name not in ['.', '..', 'build']]
            for name in filtered_names:
                child_name = os.path.join(src_dir, name)
                as_name = os.path.relpath(child_name, self.GetOption('root'))
                tar_f.add(child_name, as_name)

            build_dir = builder._GetBuildDir()
            as_name = os.path.relpath(build_dir, self.GetOption('root'))
            tar_f.add(build_dir, as_name)

        tar_f.close()

projbuilder.RuntimeBuilder._RegisterSubClass(PysideBuild)

class ShibokenBuilder(_CMakeBasedProject):

    name = 'shiboken'

    def __init__(self, config):

        git_origin = config.GIT_REPOSITORIES['shiboken']
        _CMakeBasedProject.__init__(self, config, self.name, git_origin)

    def _PostInstall(self):

        build_dir = self._GetBuildDir()
        runtime_dir = self._GetRuntimeDir()
        if sys.platform == 'win32':
            py_dst_dir = os.path.join(runtime_dir, 'lib', 'site-packages')
            assert os.path.isdir(py_dst_dir)

            for name in glob.glob(build_dir + r'\shibokenmodule\*.pdb'):
                utils.Install(name, py_dst_dir + '\\')
            for name in glob.glob(build_dir + r'\libshiboken\*.pdb'):
                utils.Install(name, runtime_dir + r'\lib')
                
projbuilder.RuntimeBuilder._RegisterSubClass(ShibokenBuilder)

class PyshibokenBuilder(ShibokenBuilder):
    
    name = 'pyshiboken'
    
projbuilder.RuntimeBuilder._RegisterSubClass(PyshibokenBuilder)

class QtBuild(projbuilder.RuntimeBuilder):

    name = 'qt'
    prefix = 'qt4'

    _separate_debug_runtime_dir = sys.platform.startswith('linux')

    def __init__(self, config, name=None):

        projbuilder.RuntimeBuilder.__init__(self, config, name)
        self._base = config

    def Clean(self):

        qt_dir = self._GetSrcDir()
        if not os.path.exists(qt_dir):
            return
        
        make_cmd = ('make' if sys.platform != 'win32' else 'nmake')
        self._RunArgvInEnv([make_cmd, 'confclean'], rundir=qt_dir)
        makefile = os.path.join(qt_dir, 'Makefile')
        if os.path.exists(makefile):
            os.remove(makefile)

    def Build(self):

        from_dist = self.GetOption('copy-from-distribution')
        if from_dist is not None:
            self._CopyFromDistribution()
            self._WriteQtConf()
            return

        src_dir = self._GetSrcDir()
        if not os.path.exists(src_dir):
            self._DownloadSource()

        # OS X note: as of 4.7.x no-framework does not work, -debug does not work, -debug-and-release does
        # Linux note: -debug-and-release does not work, so separate -debug and -release builds are needed
        qt_runtime = self._GetRuntimeDir()
        if not os.path.exists(os.path.join(src_dir, 'Makefile')):
            if sys.platform == 'win32':
                conf_argv = [src_dir + '\\configure']
            else:
                conf_argv = ['sh', 'configure']
            conf_argv += ['-opensource', '-prefix', qt_runtime.replace('/', os.sep)]
            if sys.platform.startswith('linux'):
                if self._GetDebugBuild():
                    conf_argv += ['-debug']
                else:
                    conf_argv += ['-release']
            else:
                conf_argv += ['-debug-and-release']
            extra = self.GetOption('extra-configure-args')
            if extra is not None and extra.strip() != '':
                conf_argv += [a.strip() for a in extra.split()]
            self._RunArgvInEnv(conf_argv, rundir=src_dir)

        make_cmd = ('make' if sys.platform != 'win32' else 'nmake')
        if sys.platform == 'win32':
            self._RunArgvInEnv([make_cmd], rundir=src_dir)
        else:
            self._RunArgvInEnv([make_cmd], rundir=src_dir)
        self._RunArgvInEnv([make_cmd, 'install'], rundir=src_dir)

        self._LinkDarwinIncludeDirs()
        self._WriteQtConf()
        self._InstallPdbFiles()

    def _WriteQtConf(self):

        qt_runtime = self._GetRuntimeDir()
        qt_conf = os.path.join(qt_runtime, 'bin', 'qt.conf')
        if os.path.exists(qt_conf):
            return

        f = open(qt_conf, 'w')
        f.write('[Paths]\n')
        f.write('Prefix = ..\n')
        f.close()

    def _LinkDarwinIncludeDirs(self):

        if sys.platform != 'darwin':
            return

        qt_runtime = self._GetRuntimeDir()
        lib_dir = os.path.join(qt_runtime, 'lib')
        sandbox_incl = os.path.join(qt_runtime, 'include')

        def create_link(framework_fullname):
            basename = os.path.basename(framework_fullname)
            incl = os.path.join(sandbox_incl, basename[:-len('.framework')])
            headers_dir = os.path.join(framework_fullname, 'Headers')
            if os.path.exists(incl) or not os.path.isdir(headers_dir):
                return

            relative = os.path.relpath(headers_dir, sandbox_incl)
            os.symlink(relative, incl)

        for name in os.listdir(lib_dir):
            fullname = os.path.join(lib_dir, name)
            if name.startswith('Qt') and name.endswith('.framework') \
               and os.path.isdir(fullname):
                create_link(fullname)

    def _InstallPdbFiles(self):

        if sys.platform != 'win32':
            return

        build_lib = os.path.join(self._GetSrcDir(), 'lib')
        qt_runtime = self._GetRuntimeDir()
        runtime_bin = os.path.join(qt_runtime, 'bin')

        for name in glob.glob(build_lib + '\\*.pdb'):
            base_wo_ext = os.path.basename(name[:-4])
            if os.path.exists(runtime_bin + '\\%s.dll' % base_wo_ext):
                utils.Install(name, runtime_bin)
                
projbuilder.RuntimeBuilder._RegisterSubClass(QtBuild)

class Qt5Build(projbuilder.RuntimeBuilder):

    name = 'qt5'
    prefix = 'qt5'

    _separate_debug_runtime_dir = sys.platform.startswith('linux')

    if sys.platform.startswith('linux'):
        _dependencies = ['icu', 'libxkbcommon']

    def __init__(self, config, name):
    
        projbuilder.RuntimeBuilder.__init__(self, config, name)
        self._base = config

    def _GetExistingModuleSourceDirs(self):
        """ Return list of existing directories.  The first item will be
        the qtbase directory """
        
        src_dir = self._GetSrcDir()
        if not os.path.exists(src_dir):
            return []

        qtmodule_dir_list = []
        for qtname in glob.glob(src_dir + '/qt*'):
            if os.path.isdir(qtname):
                last_part = os.path.basename(qtname)
                if last_part.startswith('qtbase'):
                    qtmodule_dir_list.insert(0, qtname)
                else:
                    qtmodule_dir_list.append(qtname)

        return qtmodule_dir_list        

    def Clean(self):
        
        module_dir_list = self._GetExistingModuleSourceDirs()
        for module_dir in module_dir_list:
            shutil.rmtree(module_dir)

    def Build(self):

        from_dist = self.GetOption('copy-from-distribution')
        if from_dist is not None:
            self._CopyFromDistribution()
            self._WriteQtConf()
            return

        src_dir = self._GetSrcDir()
        self._DownloadSource()
        self._UnpackSource()

        module_dir_list = self._GetExistingModuleSourceDirs()
        qtbase_dir = module_dir_list.pop(0)

        make_cmd = ('make' if sys.platform != 'win32' else 'nmake')

        build_qtbase = True
        if build_qtbase:
                
            # OS X note: as of 4.7.x no-framework does not work, -debug does not work, -debug-and-release does
            # Linux note: -debug-and-release does not work, so separate -debug and -release builds are needed
            qt_runtime = self._GetRuntimeDir()
            if not os.path.exists(os.path.join(qtbase_dir, 'Makefile')):
                if sys.platform == 'win32':
                    conf_argv = [qtbase_dir + '\\configure']
                else:
                    conf_argv = ['sh', 'configure']
                conf_argv += ['-opensource', '-confirm-license',
                              '-prefix', qt_runtime.replace('/', os.sep)]
                if sys.platform.startswith('linux'):
                    if self._GetDebugBuild():
                        conf_argv += ['-debug']
                    else:
                        conf_argv += ['-release']
                else:
                    conf_argv += ['-debug-and-release']
                extra = self.GetOption('extra-configure-args')
                if extra is not None and extra.strip() != '':
                    conf_argv += [a.strip() for a in extra.split()]
                self._RunArgvInEnv(conf_argv, rundir=qtbase_dir)
    
            self._RunArgvInEnv([make_cmd], rundir=qtbase_dir)
            self._RunArgvInEnv([make_cmd, 'install'], rundir=qtbase_dir)
            
        for extra_qtmodule in module_dir_list:
            self._RunArgvInEnv(['qmake'], rundir=extra_qtmodule)
            self._RunArgvInEnv([make_cmd], rundir=extra_qtmodule)
            self._RunArgvInEnv([make_cmd, 'install'], rundir=extra_qtmodule)
            
        self._WriteQtConf()
        self._InstallPdbFiles()

    def _WriteQtConf(self):

        qt_runtime = self._GetRuntimeDir()
        qt_conf = os.path.join(qt_runtime, 'bin', 'qt.conf')
        if os.path.exists(qt_conf):
            return

        f = open(qt_conf, 'w')
        f.write('[Paths]\n')
        f.write('Prefix = ..\n')
        f.close()

    def _LinkDarwinIncludeDirs(self):

        if sys.platform != 'darwin':
            return

        qt_runtime = self._GetRuntimeDir()
        lib_dir = os.path.join(qt_runtime, 'lib')
        sandbox_incl = os.path.join(qt_runtime, 'include')

        def create_link(framework_fullname):
            basename = os.path.basename(framework_fullname)
            incl = os.path.join(sandbox_incl, basename[:-len('.framework')])
            headers_dir = os.path.join(framework_fullname, 'Headers')
            if os.path.exists(incl) or not os.path.isdir(headers_dir):
                return

            relative = os.path.relpath(headers_dir, sandbox_incl)
            os.symlink(relative, incl)

        for name in os.listdir(lib_dir):
            fullname = os.path.join(lib_dir, name)
            if name.startswith('Qt') and name.endswith('.framework') \
               and os.path.isdir(fullname):
                create_link(fullname)

    def _InstallPdbFiles(self):

        if sys.platform != 'win32':
            return

        build_lib = os.path.join(self._GetSrcDir(), 'lib')
        qt_runtime = self._GetRuntimeDir()
        runtime_bin = os.path.join(qt_runtime, 'bin')

        for name in glob.glob(build_lib + '\\*.pdb'):
            base_wo_ext = os.path.basename(name[:-4])
            if os.path.exists(runtime_bin + '\\%s.dll' % base_wo_ext):
                utils.Install(name, runtime_bin)

    def _GetEnvChanges(self, base_ok=False):
        """ Returns dict of changes to make to the environment.  Note that any
        variable name ending in PATH will be prepending to the existing value. 
        """
        
        changes = super(Qt5Build, self)._GetEnvChanges(base_ok)
        if sys.platform == 'win32':
            changes['PATH'] += ';' + os.path.dirname(sys.executable)
        
        return changes

    def _DownloadTarball(self, url):
        
        src_dir = self._GetSrcDir()
        if not os.path.exists(src_dir):
            return None
        
        retr = utils.RetrievalOperations(self.config)
        
        if url.startswith('file:') and os.sep != '/':
            url = url.replace(os.sep, '/')
        last_part = url.split('/')[-1]
        local_file = os.path.join(src_dir, last_part)

        if not os.path.exists(local_file):
            retr.Download(url, local_file)
        
        return local_file
        
    def _DownloadSource(self):
        """ Download source -- complicated by qt5 multiple submodules """
        
        src_dir = self._GetSrcDir()
        if not os.path.exists(src_dir):
            os.makedirs(src_dir)
        
        info = self._GetRepositoryInfo()
        url = info.url
        
        local_file = self._DownloadTarball(info.url)
        env = os.environ.copy()
        if sys.platform == 'win32':
            env['PATH'] += ';' + os.path.dirname(self.config.GIT)
            env['PATH'] += ';' + os.path.dirname(sys.executable)

        extra_str = self.GetOption('extra-qt-modules')
        if extra_str is not None and extra_str.strip() != '':
            extra_list = [a.strip() for a in extra_str.split() if a.strip() != '']
        else:
            extra_list = []
            
        for extra in extra_list:
            local_file = self._DownloadTarball(extra)
            
            
    def _UnpackSource(self, delete_first=False):
        
        src_dir = self._GetSrcDir()
        if not os.path.exists(src_dir):
            os.makedirs(src_dir)
        
        env = os.environ.copy()
        if sys.platform == 'win32':
            env['PATH'] += ';' + os.path.dirname(self.config.GIT)
            env['PATH'] += ';' + os.path.dirname(sys.executable)

        name_list = glob.glob(src_dir + '/qt*')
        for name in name_list:

            if '.tar' in os.path.basename(name):
                mod_dirname = utils.StripExt(name)
                if not os.path.exists(mod_dirname):
                    self._RunArgvInEnv(['tar', '-xvf', name], rundir=src_dir,
                                       env=env)
            elif name.endswith('.zip'):
                mod_dirname = utils.StripExt(name)
                if not os.path.exists(mod_dirname):
                    utils.UnpackZip(name, mod_dirname)

        self._ApplyPatch()
            
projbuilder.RuntimeBuilder._RegisterSubClass(Qt5Build)

class ScintillaEditPyBuild(projbuilder.RuntimeBuilder):
    """ Build qt ScintillaEdit C++ component and ScintillaEditPy pyside
    based binding """

    name = 'scintillaeditpy'
    _separate_debug_runtime_dir = True
    _separate_pyversion_runtime_dir = True

    def __init__(self, config, name=None):

        projbuilder.RuntimeBuilder.__init__(self, config)
        target_python = self._GetTargetPython()
        target_qt = self.GetOption('target-qt', default='qt')

        self._dependencies = ['cmake', target_python.proj_name, 'pyside']
        if target_qt != 'system-qt':
            self._dependencies.append(target_qt)
        self._runtime_dir_suffixes = [target_python.runtime_suffix]
        if target_qt not in ('qt', 'system-qt'):
            self._runtime_dir_suffixes.append(target_qt)

    def Clean(self):

        try:
            self._RunSepBuild(['--clean'])
        except OSError:
            pass

    def Build(self):

        src_dir = self._GetSrcDir()
        if not os.path.exists(src_dir):
            self._DownloadSource()

        dbg_arg = ('--debug=yes' if self._GetDebugBuild() else '--debug=no')
        self._RunSepBuild([dbg_arg, '--underscore-names'])

        self._Install()

    def _Install(self):

        # Put the files in build-dir/bin into lib
        build_bin_dir = os.path.join(self._GetSrcDir(), 'bin')
        install_dir = self._GetRuntimeDir()
        if sys.platform == 'win32':
            py_lib_install_dir = os.path.join(install_dir, 'lib',
                                              'site-packages')
        else:

            py_lib_install_dir = os.path.join(install_dir, 'lib',
                                              'python' + self._GetTargetPython().version,
                                              'site-packages')

        if not os.path.exists(py_lib_install_dir):
            os.makedirs(py_lib_install_dir)

        dont_copy = ('.cvsignore', 'empty.txt')
        for name in [n for n in os.listdir(build_bin_dir) if n not in dont_copy]:
            src = os.path.join(build_bin_dir, name)
            dest = os.path.join(py_lib_install_dir, name)
            if (not os.path.exists(dest)
                or os.stat(src).st_mtime > os.stat(dest).st_mtime):
                if not os.path.islink(src):
                    shutil.copy(src, dest)
                else:
                    link = os.readlink(src)
                    assert not os.path.isabs(link) and len(link.split('/')) == 1
                    if os.path.exists(dest):
                        os.remove(dest)
                    os.symlink(link, dest)

    def _RunSepBuild(self, args):

        src_dir = self._GetSrcDir()
        scint_build_dir = os.path.join(src_dir, 'qt', 'ScintillaEditPy')

        cmdv = [self._GetTargetPython().executable, 'sepbuild.py']
        cmdv.extend(args)

        pyside_builder = projbuilder.GetBuilderForName('pyside', self.config)
        cmdv.append('--pyside-base=%s' % pyside_builder._GetRuntimeDir())

        self._RunArgvInEnv(cmdv, rundir=scint_build_dir, check=True)

projbuilder.RuntimeBuilder._RegisterSubClass(ScintillaEditPyBuild)

class CMakeBuild(projbuilder.RuntimeBuilder):

    URL = 'http://www.cmake.org/files/v2.8/cmake-2.8.12.tar.gz'
    
    name = 'cmake'

    def __init__(self, config):

        projbuilder.RuntimeBuilder.__init__(self, config)

        local_name = self.URL.split('/')[-1]
        self.build_dir = self.GetOption('root') + '/cmake/' + utils.StripExt(local_name)
        self.dist_file = self.GetOption('root') + '/cmake/' + local_name

    def _GetSrcDir(self):
        
        return self.build_dir

    def Build(self):

        retr = utils.RetrievalOperations(self.config)
        retr.Download(self.URL, self.dist_file)
        if not os.path.exists(self.build_dir):
            utils.UnpackTar(self.dist_file, self.build_dir)

        if not os.path.exists(self.build_dir + '/Makefile'):
            bootstrap_argv = ['sh', './bootstrap',
                              '--prefix=%s' % self._GetRuntimeDir()]
            self._RunArgvInEnv(bootstrap_argv, self.build_dir)

        self._RunArgvInEnv(['make'], self.build_dir)
        self._RunArgvInEnv(['make', 'install'], self.build_dir)

    def Clean(self):

        if os.path.exists(self.build_dir + '/Makefile'):
            self._RunArgvInEnv(['make', 'clean'], self.build_dir)

projbuilder.RuntimeBuilder._RegisterSubClass(CMakeBuild)



