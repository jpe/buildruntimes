# Copyright (c) 2001-2012, Archaeopteryx Software, Inc.  All rights reserved.
#
# Written by John P. Ehresman and Stephan R.A. Deibel
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import glob
import os
import sys
import shutil
import tarfile
try:
    import multiprocessing
except ImportError:
    multiprocessing = None

from buildruntimes import projbuilder
from buildruntimes import utils


class SipBuilder(projbuilder.RuntimeBuilder):
    
    name = 'sip'
    
    _separate_debug_runtime_dir = True
    
    def __init__(self, config):
        
        projbuilder.RuntimeBuilder.__init__(self, config)
    
        self._runtime_dir_suffixes = [self._GetTargetPython().runtime_suffix]

    def _GetSrcDir(self):

        return os.path.join(super(SipBuilder, self)._GetSrcDir(), self.name)
    
    def Build(self):
        """ Build and install into runtime directory """

        src_dir = self._GetSrcDir()
        if not os.path.exists(src_dir):
            self._DownloadSource()
        
        self._Configure()

        make_cmd = ('make' if sys.platform != 'win32' else 'nmake')
        argv = [make_cmd, 'install']
        self._RunArgvInEnv(argv, rundir=src_dir)

    def _Configure(self):
        
        src_dir = self._GetSrcDir()
        dest_dir = self._GetRuntimeDir()

        target_python = self._GetTargetPython()

        pylib_dir = os.path.join(dest_dir, target_python.relative_site_packages) 
        argv = [target_python.executable, 'configure.py',
                '--bindir', dest_dir,
                '--incdir', dest_dir + '/include',
                '--destdir', pylib_dir,
        ]
        if self._GetDebugBuild():
            argv.append('--debug')

        print argv, src_dir
        self._RunArgvInEnv(argv, rundir=src_dir)

    def Clean(self):
        """ Clean intermediate files """
        
        src_dir = self._GetSrcDir()
        
        if os.path.exists(src_dir + '/Makefile'):
            self._RunMakeInEnv(['clean'], src_dir)        
            os.remove(src_dir + '/Makefile')

projbuilder.RuntimeBuilder._RegisterSubClass(SipBuilder)
class CommonPyQtBuilder(projbuilder.RuntimeBuilder):

    configure_script = None

    def _UsePrivateSip(self):
        
        private_sip = self.GetOption('private-sip-tarball-url')
        return (private_sip is not None)

    def _GetSrcDir(self):

        return os.path.join(super(CommonPyQtBuilder, self)._GetSrcDir(), self.name)
    
    def _GetSipSrcDir(self):
        
        pyqt_src_dir = self._GetSrcDir()
        return os.path.join(os.path.dirname(pyqt_src_dir), 'sip')        
    
    def _DownloadSource(self):
        
        src_dir = self._GetSrcDir()
        if not os.path.exists(src_dir):
            super(CommonPyQtBuilder, self)._DownloadSource()
        
        private_sip = self.GetOption('private-sip-tarball-url')
        if private_sip is None:
            return
        
        parent_dir = os.path.dirname(self._GetSrcDir())
        
        retr = utils.RetrievalOperations(self.config)
        sip_tarball = retr.DownloadToDir(private_sip, parent_dir)
        sip_dirname = self._GetSipSrcDir()
        if not os.path.exists(sip_dirname):
            utils.UnpackTar(sip_tarball, sip_dirname)
    
    def Build(self):
        """ Build and install into runtime directory """

        self._DownloadSource()
        
        self._PipInstallEnum34()
        self._BuildSip()
        self._BuildPyQt()
        
    def _Configure(self):
        self._ConfigureSip()
        self._ConfigurePyQt()
        
    def _PipInstallEnum34(self):
        
        if self.GetOption('pip-install-enum34') != 'yes':
            return
            
        parent_dir = os.path.dirname(self._GetSrcDir())
        pip_build_dir = os.path.join(parent_dir, 'pip-build')
        
        if not os.path.isdir(pip_build_dir):
            os.makedirs(pip_build_dir)
            
        target_python = self._GetTargetPython()
        runtime_dir = self._GetRuntimeDir()
        pip_packages = ['enum34']

        pip_install_argv = ['install'] + pip_packages + ['--prefix', runtime_dir]
        cmdv = [target_python.executable, '-m', 'pip'] + pip_install_argv
        self._RunArgvInEnv(cmdv, pip_build_dir)
        
    def _BuildSip(self):
        
        if not self._UsePrivateSip():
            return
        
        self._ConfigureSip()

        self._RunMakeInEnv(['install'], rundir=self._GetSipSrcDir())
        
    def _ConfigureSip(self):
        
        sip_dirname = self._GetSipSrcDir()
        if not os.path.exists(sip_dirname):
            return
        
        dest_dir = self._GetRuntimeDir()
        bin_dir = (dest_dir + '/bin' if sys.platform != 'win32'
                   else dest_dir + '/Scripts')

        target_python = self._GetTargetPython()

        pylib_dir = os.path.join(dest_dir, target_python.relative_site_packages) 
        argv = [target_python.executable, 'configure.py',
                '--bindir', bin_dir,
                '--incdir', dest_dir + '/include',
                '--destdir', pylib_dir,
                '--sip-module', 'PyQt5.sip',
                '--no-dist-info',
        ]
        if self._GetDebugBuild():
            argv.append('--debug')

        self._RunArgvInEnv(argv, rundir=sip_dirname)

    def _BuildPyQt(self):
        
        self._ConfigurePyQt()
        self._RunMakeInEnv(['install'], rundir=self._GetSrcDir())
        
    def _ConfigurePyQt(self):
        """ Configure and generate wrapper sources """
        
        src_dir = self._GetSrcDir()
        dest_dir = self._GetRuntimeDir()

        if os.path.exists(os.path.join(src_dir, 'Makefile')):
            return

        if self._UsePrivateSip():
            sip_rt_dir = dest_dir
            sip_binary = (sip_rt_dir + (r'\Scripts\sip.exe' if sys.platform == 'win32' 
                                        else '/bin/sip'))
        else:
            sip_builder = projbuilder.GetBuilderForName('sip', self.config)

            sip_rt_dir = sip_builder._GetRuntimeDir()
            sip_binary = (sip_rt_dir + (r'\sip.exe' if sys.platform == 'win32' 
                                        else '/sip'))
        
        dest_sip_dir = dest_dir + '/sip'

        target_python = self._GetTargetPython()

        pylib_dir = os.path.join(dest_dir, target_python.relative_site_packages)
        argv = [target_python.executable, self.configure_script,
                '--bindir', dest_dir,
                '--sipdir', dest_sip_dir,
                '--destdir', pylib_dir,
                '--sip-incdir', sip_rt_dir + '/include',
                '--sip', sip_binary,
                '--confirm-license',
                '--no-designer-plugin',
                '--no-tools',
                '--no-dist-info',
        ]

        if self._GetDebugBuild():
            argv.append('--debug')

        extra = self.GetOption('extra-configure-args')
        if extra is not None and extra.strip() != '':
            argv += [a.strip() for a in extra.split()]

        self._RunArgvInEnv(argv, rundir=src_dir)

    def Clean(self):
        """ Clean intermediate files """

        src_dir = self._GetSrcDir()
        if os.path.exists(src_dir + '/Makefile'):
            self._RunMakeInEnv(['distclean'], src_dir)
            
        sip_src_dir = self._GetSipSrcDir()
        if os.path.exists(sip_src_dir + '/Makefile'):
            self._RunMakeInEnv(['clean'], sip_src_dir)        
            os.remove(sip_src_dir + '/Makefile')

class PyQt4Builder(CommonPyQtBuilder):
    
    name = 'pyqt4'
        
    _separate_debug_runtime_dir = True
    
    configure_script = 'configure-ng.py'
    
    def __init__(self, config):
        
        projbuilder.RuntimeBuilder.__init__(self, config)
    
        target_python = self._GetTargetPython()
        self._dependencies = ['qt', target_python.proj_name, 'sip']
        self._runtime_dir_suffixes = [target_python.runtime_suffix]

            
projbuilder.RuntimeBuilder._RegisterSubClass(PyQt4Builder)


class PyQt5Builder(PyQt4Builder):
    
    name = 'pyqt5'
    
    configure_script = 'configure.py'
    
    def __init__(self, config):
        
        projbuilder.RuntimeBuilder.__init__(self, config)
    
        target_python = self._GetTargetPython()
        
        target_qt_name = self.GetOption('target-qt', default='qt5.5')
        
        self._dependencies = [target_qt_name, target_python.proj_name, 'openssl']
        if not self._UsePrivateSip():
            self._dependencies.append('sip')
        if sys.platform.startswith('linux'):
            self._dependencies.append('icu')
        self._runtime_dir_suffixes = [target_python.runtime_suffix, target_qt_name]

projbuilder.RuntimeBuilder._RegisterSubClass(PyQt5Builder)

class ScintillaEditBuilder(projbuilder.RuntimeBuilder):
    
    name = 'scintillaedit'
    
    _separate_debug_runtime_dir = True
    
    def __init__(self, config, name=None):
        
        projbuilder.RuntimeBuilder.__init__(self, config)

        target_qt_name = self.GetOption('target-qt', default='qt5.5')
    
        self._dependencies = [target_qt_name]
        self._runtime_dir_suffixes = [target_qt_name]
    
    def Build(self):
        """ Build and install into runtime directory """

        src_dir = self._GetSrcDir()
        if not os.path.exists(src_dir):
            self._DownloadSource()
        
        self._Configure()

        scintilla_edit_dir = os.path.join(src_dir, 'qt', 'ScintillaEdit')
        config_arg = ('CONFIG+=debug' if self._GetDebugBuild() 
                      else 'CONFIG+=release')
        argv = ['qmake', 'ScintillaEdit.pro', config_arg]
        self._RunArgvInEnv(argv, rundir=scintilla_edit_dir)

        if sys.platform == 'win32':
            argv = (['debug'] if self._GetDebugBuild() else [])
        else:
            argv = []
        self._RunMakeInEnv(argv, rundir=scintilla_edit_dir)
        
        self._CopyToRuntime()

    def _Configure(self):

        src_dir = self._GetSrcDir()
        scintilla_edit_dir = os.path.join(src_dir, 'qt', 'ScintillaEdit')

        target_python = self._GetTargetPython()
        
        argv = [target_python.executable, 'WidgetGen.py',
                '--underscore-names']
        self._RunArgvInEnv(argv, rundir=scintilla_edit_dir)

    def _CopyToRuntime(self):
        
        src_dir = self._GetSrcDir()
        rt_dir = self._GetRuntimeDir()
        
        if sys.platform == 'win32':
            bin_name_list = ['ScintillaEdit3.dll', 'ScintillaEdit3.lib']
            if os.path.exists(src_dir + r'\bin\ScintillaEdit3.pdb'):
                bin_name_list.append('ScintillaEdit3.pdb')
            shlib_dest = rt_dir + '\\bin\\'
        elif sys.platform == 'darwin':
            bin_name_list = ['ScintillaEdit.framework']
            shlib_dest = rt_dir + '/lib/ScintillaEdit.framework/'
        else:
            bin_name_list = [os.path.basename(n) 
                             for n in glob.glob(src_dir + '/bin/lib*.so*')]
            shlib_dest = rt_dir + '/lib/'
        
        for name in bin_name_list:
            utils.Install(src_dir + '/bin/' + name, shlib_dest)

        if sys.platform == 'darwin':
            # Need to change the install_name from the default 
            # @executable_path/../Frameworks/...
            relative_name = 'ScintillaEdit.framework/Versions/3/ScintillaEdit'
            default_install_name = '@executable_path/../Frameworks/' + relative_name
            installed_name = os.path.join(rt_dir, 'lib', relative_name)
            cmdv = ['install_name_tool', '-id', installed_name, installed_name]
            self._RunArgvInEnv(cmdv, shlib_dest)
            cmdv = ['install_name_tool', '-id', installed_name, installed_name]
            self._RunArgvInEnv(cmdv, shlib_dest)
            
        # Copy ScintillConstants.py
        target_py = self._GetTargetPython()
        rt_sitepackages = os.path.join(rt_dir, target_py.relative_site_packages)
        constants_py = src_dir + '/qt/ScintillaEditPy/ScintillaConstants.py'
        utils.Install(constants_py, rt_sitepackages + os.sep)
            
        include_list = ['include/*.h', 'qt/ScintillaEdit/*.h',
                        'qt/ScintillaEditBase/ScintillaEditBase.h']
        for pattern in include_list:
            for name in glob.glob(os.path.join(src_dir, pattern)):
                utils.Install(name, rt_dir + '/include/')
        
    def Clean(self):
        """ Clean intermediate files """

        src_dir = self._GetSrcDir()
   
        sciedit_dir = os.path.join(src_dir, 'qt', 'ScintillaEdit')
        if os.path.exists(sciedit_dir + '/Makefile'):
            self._RunMakeInEnv(['distclean'], rundir=sciedit_dir)
           
        to_remove_list = [
            'qt/ScintillaEdit/ScintillaEdit.h',
            'qt/ScintillaEdit/ScintillaEdit.cpp',
            'qt/ScintillaEditPy/ScintillaConstants.py',
        ]   
        for name in to_remove_list:
            fullname = os.path.join(src_dir, name)
            if os.path.exists(fullname):
                os.remove(fullname)

projbuilder.RuntimeBuilder._RegisterSubClass(ScintillaEditBuilder)
