# Copyright (c) 2001-2012, Archaeopteryx Software, Inc.  All rights reserved.
#
# Written by John P. Ehresman and Stephan R.A. Deibel
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import ConfigParser as configparser
import glob
import hashlib
import os
import sys
import shutil
import subprocess
import tarfile
import zipfile

from buildruntimes import utils


class RuntimeBuilder(object):
    """ Base class to build a runtime; meant to be subclassed, though
    many of the methods have useful implementations. """

    ALL_BUILDER_CLASSES = {}
    """ Registry of project names to classes to build them """

    name = None
    """ Name of project. """

    prefix = None
    """ Prefix for builder that handles multiple names """

    _separate_debug_runtime_dir = False
    """ Whether separate dir tree is needed for debug v release builds.
    Often overridden in subclasses. """

    _separate_pyversion_runtime_dir = False
    _runtime_dir_suffixes = []
    """ Whether separate dir tree is needed for each python version.
    Often overridden in subclasses. """
    
    _dependencies = ()
    """ Names of projects that this project is dependant on """
    
    @classmethod
    def _RegisterSubClass(cls, sub):

        if sub.name is not None:
            cls.ALL_BUILDER_CLASSES[sub.name] = sub
        if sub.prefix is not None:
            cls.ALL_BUILDER_CLASSES[sub.prefix] = sub

    def __init__(self, config, name=None):

        self.config = config
        self.__cached_target_python_version = None
        
        if name is not None:
            self.name = name
        
        plat = self.config.PLATFORM
        d_section = self.config.ini_file.default_section       
        self._ini_sections = [
            self.name + ':' + plat, 
            self.name, 
            d_section + ':' + plat, 
            d_section
        ]
        
    def _ExpandDollarVariables(self, value, being_expanded=()):
        """ Expand any $name variable """
        
        if not isinstance(value, basestring):
            return value

        def find_name(value, dollar_pos):
            if dollar_pos + 1 < len(value) and value[dollar_pos + 1] == '{':
                end_pos = value.find('}', dollar_pos + 2)
                if end_pos != -1:
                    return value[dollar_pos + 2:end_pos], end_pos + 1
            else:
                for end_pos in range(dollar_pos + 1, len(value)):
                    if not value[end_pos].isalnum():
                        return value[dollar_pos + 1:end_pos], end_pos

            return value[dollar_pos + 1:], len(value)
                    
        dollar_pos = value.find('$')
        while dollar_pos != -1:
            name, end_pos = find_name(value, dollar_pos)
            sub_value = self.GetOption(name)
            value = value[:dollar_pos] + sub_value + value[end_pos:]
            dollar_pos = value.find('$')
            
        return value
            
    def GetOption(self, name, arg_options=None, default=None):
        """ Get option value as set either in the .ini file or found
        on the command line. """

        if arg_options is not None:
            val = arg_options.get('--' + name)
            if val is not None:
                return val
            
        if name in self.config.arg_options:
            return self.config.arg_options[name]

        def get_from_ini(section_name_list):
            for section_name in section_name_list:
                try:
                    return self.config.ini_file.get(section_name, name, raw=0)
                except configparser.NoOptionError:
                    pass
                except configparser.NoSectionError:
                    pass

            return default

        value = get_from_ini(self._ini_sections)
 
        return self._ExpandDollarVariables(value)
    
    def _GetSrcDir(self):
        """ Returns name of source code directory. """
        
        from_dist = self.GetOption('copy-from-distribution')
        if from_dist is not None:
            return None
        
        return os.path.join(self.GetOption('root'), self.name)

    def _GetRuntimeDir(self, base_ok=False):
        """ Get the runtime directory name """

        local_name = 'runtime-' + self.name
        for suffix in self._runtime_dir_suffixes:
            local_name += '-' + suffix
        if self._separate_debug_runtime_dir:
            suffix = ('-debug' if self._GetDebugBuild() else '-release')
            local_name += suffix

        build_number = self.GetOption('build-number')
        if build_number is not None:
            local_name += '-%s' % build_number
            
        in_this_root = os.path.join(self.GetOption('root'), 'runtimes', 
                                    local_name)
        if (not os.path.isdir(in_this_root) and base_ok
              and (self._GetSrcDir() is None
                   or not os.path.isdir(self._GetSrcDir()))):
            base_runtime_option = self.GetOption('base-runtime-dirs', default='')
            base_root_list = [b.strip() for b in base_runtime_option.split(';')
                              if b.strip() != '']
            for base_root in base_root_list:
                in_base_root = os.path.join(base_root, local_name)
                if os.path.exists(in_base_root):
                    return in_base_root
                
        return in_this_root

    class PythonInfo:
        def __init__(self, executable, version, runtime_suffix, proj_name):
            self.executable = executable
            self.version = version
            self.runtime_suffix = runtime_suffix
            self.proj_name = proj_name
            
            rsp = (r'Lib\site-packages' if sys.platform == 'win32'
                   else 'lib/python%s/site-packages' % (version[:3],))            
            self.relative_site_packages = rsp
            
    def _GetTargetPython(self):
        """ Get object with info about the target python.  Returned object
        will have executable, version, ... fields. """
        
        target_python = self.GetOption('target-python')
        if target_python is not None:
            py_builder = GetBuilderForName(target_python, self.config)
            if py_builder is not None:
                info = py_builder._GetTargetPython()
                return info

        if target_python is None:
            target_python = (sys.executable if sys.platform == 'win32'
                             else 'python2.7')

        # Use sys.executable on win32 if right version
        if sys.platform == 'win32' and not os.path.isabs(target_python):
            if target_python == 'python':
                target_python = sys.executable
            elif target_python.startswith('python'):
                suffix = target_python[-3:]
                if (suffix[0].isdigit() and suffix[1] == '.'
                    and suffix[2].isdigit()
                    and self._GetPythonVersion(sys.executable) == suffix):
                    target_python = sys.executable
                    
        if not os.path.isabs(target_python):
            py_builder = GetBuilderForName(target_python, self.config)
            target_python = os.path.join(py_builder._GetRuntimeDir(base_ok=True),
                                         'bin', target_python)
            
        version = self._GetPythonVersion(target_python)
            
        return self.PythonInfo(executable=target_python, version=version,
                               runtime_suffix='py'+version,
                               proj_name='python'+version)

    def _GetPythonVersion(self, executable):
        
        suffix = executable[-3:]
        if (len(suffix) == 3 and suffix[0].isdigit() and suffix[1] == '.'
            and suffix[2].isdigit()):
            py_version = suffix
        else:
            import subprocess
            output = subprocess.check_output([executable, '-c',
                                              'import sys; print (sys.version[:3])'])
            py_version = output.splitlines()[0].strip()
            
        return py_version
    
    def _GetDebugBuild(self):
        """ Get boolean for whether this is a debug build. """
        
        dbg_flag = self.GetOption('debug', default='yes')
        if dbg_flag == 'no':
            return False
        else:
            return True

    def _DownloadSource(self):

        src_dir = self._GetSrcDir()
        if os.path.exists(src_dir):
            return
        
        info = self._GetRepositoryInfo()
        retr = utils.RetrievalOperations(self.config)
        retr.RetrieveIntoDir(info, src_dir)
        
    def _ApplyPatch(self):
        """ Apply any patch -- from the patch-cmd """
        
        patch_cmd = self.GetOption('patch-cmd')
        if patch_cmd is None:
            return
        
        src_dir = self._GetSrcDir()
        if sys.platform == 'win32':
            git_install_dir = os.path.dirname(os.path.dirname(self.config.GIT))
            patch_exe = os.path.join(git_install_dir, 'usr', 'bin', 'patch.exe')
            patch_cmd = os.path.expandvars(patch_cmd)
            if not patch_cmd.startswith('patch ') or '"' in patch_cmd:
                raise NotImplementedError('win32 patch cmd must begin with patch')
            patch_cmd = patch_cmd[len('patch '):]
     
            patch_argv = [patch_exe] + patch_cmd.split(' ')
        else:
            patch_argv = ['sh', '-c', patch_cmd]

        self._RunArgvInEnv(patch_argv, src_dir)
        
    def _GetRepositoryInfo(self):
        """ Get the origin info for this project """

        url = self.GetOption('origin-url')
        branch = self.GetOption('origin-branch')
        if url is not None:
            info = utils.RetrievalOperations.Info()
            info.url = url
            info.branch = self.GetOption('origin-branch')
            info.tag = self.GetOption('origin-tag')
            info.kind = self.GetOption('origin-kind')
            if info.kind is None:
                if url.startswith('git:') or '.git' in url:
                    info.kind = 'git'
                else:
                    info.kind = 'hg'

            return info
        
        info = self.config.GetSourceRepository(self.name)
        if info is not None:
            return info
        
        tarball_url = self.GetOption('source-tarball-url')
        if tarball_url is not None:
            info = utils.RetrievalOperations.Info()
            info.url = tarball_url
            info.kind = 'tarball'
            return info

        return None

    def _CopyFromDistribution(self):
        """ Copy the tree from the directory specified by copy-from-distribution
        into the runtime directory. """

        dir_name = self.GetOption('copy-from-distribution')
        if dir_name is None:
            return
        
        dest_dir = self._GetRuntimeDir()
        utils.Install(dir_name, dest_dir)
            
    def Build(self):
        """ Build and install into runtime directory """

        raise NotImplementedError()

    def Clean(self):
        """ Clean intermediate files """

        raise NotImplementedError()

    def Archive(self):
        """ Archive the runtime dir into a .tar.bz2 file and write a 
        .runtime-info file for it with the sha1 hash """

        runtime_dir = self._GetRuntimeDir()
        
        if not os.path.exists(runtime_dir):
            print ("Runtime directory doesn't exist")
            return

        parent_dir = os.path.dirname(runtime_dir)
        basename = os.path.basename(runtime_dir)

        archive_name = os.path.join(parent_dir, basename + '.tar.bz2')
        tar_f = tarfile.open(archive_name, 'w:bz2')
        tar_f.add(runtime_dir, '.')
        tar_f.close()
        
        self._WriteRuntimeInfo(archive_name)
        
    def _GetBuildRuntimesGitVersion(self):
        """ Returns git id of buildruntimes as a string or None if
        it's not available. """
                
        buildruntimes_dir = os.path.join(self.GetOption('root'), 'buildruntimes')
        cmdv = [self.config.GIT, 'rev-parse', 'HEAD']
        try:
            rev_parse_output = subprocess.check_output(cmdv, cwd=buildruntimes_dir)
        except:
            return None

        cmdv = [self.config.GIT, 'diff']
        try:
            diff_output = subprocess.check_output(cmdv, cwd=buildruntimes_dir)
        except:
            return None

        git_hash = rev_parse_output.strip()
        if diff_output.strip() != '':
            git_hash += '+'
            
        return git_hash
        
    def _WriteRuntimeInfo(self, archive_name):
        """ Write the .runtime-info file for the given archive file. """

        info_name = archive_name + '.runtime-info'
        
        kBlockSize = 10 * 1024 * 1024
        sha1_hash = hashlib.sha1()
        f = open(archive_name, 'rb')
        block = None
        while block != '':
            block = f.read(kBlockSize)
            sha1_hash.update(block)
        f.close()
        
        comment = self.GetOption('comment')
        buildruntimes_version = self._GetBuildRuntimesGitVersion()
        
        info_f = open(info_name, 'w')
        try:
            info_f.write('sha1sum = %s\n' % (sha1_hash.hexdigest(),))

            if buildruntimes_version is not None:
                info_f.write('buildruntimes-git-version = {}\n'.format(
                    buildruntimes_version))

            if comment is not None:
                comment_line_list = comment.splitlines()
                for i, line in enumerate(comment_line_list):
                    if i == 0:
                        info_f.write('comment = %s\n' % (line,))
                    else:
                        info_f.write('          %s\n' % (line,))
        finally:
            info_f.close()

    def Expand(self, archive_name=None, dirname=None):

        if dirname is None:
            dirname = self._GetRuntimeDir()

        if archive_name is None:
            archive_name = self._GetRuntimeDir() + '.tar.bz2'

        tar_f = tarfile.open(archive_name, 'r:bz2')
        tar_f.extractall(dirname)
        tar_f.close()

    def ArchiveDebugFiles(self, archive_name=None):

        src_dir = self._GetSrcDir()
        if src_dir is None or not os.path.exists(src_dir):
            return
        
        runtime_dir = self._GetRuntimeDir()
        if archive_name is None:
            archive_name = runtime_dir + '-debugfiles.tar.bz2'

        tar_f = tarfile.open(archive_name, 'w:bz2')
        as_name = os.path.relpath(src_dir, self.GetOption('root'))
        tar_f.add(src_dir, as_name)
        tar_f.close()

    def ExpandDebugFiles(self, archive_name=None):

        if os.path.isdir(self._GetSrcDir()):
            print 'Not expanding debugfiles for:', self.name
            return

        runtime_dir = self._GetRuntimeDir()
        if archive_name is None:
            archive_name = runtime_dir + '-debugfiles.tar.bz2'

        tar_f = tarfile.open(archive_name, 'r:bz2')
        tar_f.extractall(self.GetOption('root'))
        tar_f.close()
        
    def UpdateSource(self):
        """ Update source from repository. """
        
        src_dir = self._GetSrcDir()
        if src_dir is None:
            print ("No source directory")
        elif not os.path.exists(src_dir):
            print ("Directory does not exist: %s" % src_dir)
        else:
            info = self._GetRepositoryInfo()
            if info is not None:
                retr = utils.RetrievalOperations(self.config)
                retr.UpdateDir(info, src_dir)
        
        
    def _GetEnvChanges(self, base_ok=False):
        """ Returns dict of changes to make to the environment.  Note that any
        variable name ending in PATH will be prepended to the existing value. 
        """
        
        runtime_dir = self._GetRuntimeDir(base_ok=base_ok)
        
        changes = {}
        if sys.platform == 'win32':
            changes['PYTHONPATH'] = '%s\\lib\\site-packages' % runtime_dir
            if os.path.exists(runtime_dir + '\\bin'):
                changes['PATH'] = runtime_dir + '\\bin'
            else:
                changes['PATH'] = runtime_dir
        else:
            changes['PYTHONPATH'] = ('%s/lib/python%s/site-packages'
                                     % (runtime_dir, self._GetTargetPython().version))
            changes['PATH'] = os.path.join(runtime_dir, 'bin')
            changes['CPATH'] = os.path.join(runtime_dir, 'include')
            changes['LIBRARY_PATH'] = os.path.join(runtime_dir, 'lib')

        if sys.platform == 'darwin':
            changes['DYLD_LIBRARY_PATH'] = '%s/lib' % runtime_dir
            changes['DYLD_FRAMEWORK_PATH'] = '%s/lib' % runtime_dir
        elif sys.platform != 'win32': # Linux and other unixlike
            changes['LD_LIBRARY_PATH'] = '%s/lib' % runtime_dir
            changes['PKG_CONFIG_PATH'] = '%s/lib/pkgconfig' % runtime_dir
        
        return changes
    
    def _RunArgvInEnv(self, cmdv, rundir, include_self=True, check=True,
                      env=None):

        if env is None:
            env = os.environ.copy()
        
        def prepend_path(varname, path):
            path = os.path.normpath(path)

            orig = env.get(varname)
            env[varname] = path
            if orig:
                env[varname] += os.pathsep + orig
                
        # XXX temporary
        import buildruntimes

        if sys.platform == 'darwin':
            set_cflags = self.GetOption('set-cflags-to-macos-sdk-include')
            if set_cflags == 'yes':
                sdk_path = subprocess.check_output(['xcrun', '--show-sdk-path'])
                env['CFLAGS'] = "-I{}/usr/include".format(sdk_path.strip())

        builder_list = [GetBuilderForName(name, self.config) for name
                        in self._dependencies]
        if include_self and not any(b.name == self.name for b in builder_list):
            builder_list.append(self)
            
        for builder in builder_list:
            changes = builder._GetEnvChanges(base_ok=(builder != self))
            for name, value in changes.items():
                if name.endswith('PATH'):
                    prepend_path(name, value)
                else:
                    env[name] = value

        if self.config.PERL_BIN_DIR is not None:
            prepend_path('PATH', self.config.PERL_BIN_DIR)

        if self.config.GIT_SSH is not None:
            env['GIT_SSH'] = self.config.GIT_SSH
 
        if sys.platform != 'win32':
            return utils.RunArgv(cmdv, rundir, env=env, check=check)
        else:
            vs_name = self.GetOption('vs-name', default='vs2008')
            vs_info = self.config.VS_INFO[vs_name.lower()]
            
            quoted_argv = [('"%s"'% a if ' ' in a else a) for a in cmdv]
            cmd_line = ('cmd.exe /s /c ""%s" && ( %s )"'
                        % (vs_info.vcvars_bat, ' '.join(quoted_argv)))
            return utils.RunArgv(cmd_line, rundir, env=env, check=check)

    def _RunMakeInEnv(self, argv, rundir, include_self=True, check=True,
                      env=None):
        """ Run make or nmake.  Arguments are the same for _RunArgvInEnv """
        
        make_cmd = ('make' if sys.platform != 'win32' else 'nmake')
        cmdv = [make_cmd] + argv
        self._RunArgvInEnv(cmdv, rundir, include_self=include_self, check=check, 
                           env=env)
        
def GetBuilderForName(name, config):
    
    cls = RuntimeBuilder.ALL_BUILDER_CLASSES.get(name)
    if cls is not None:
        return cls(config)

    for cls_id, cls in RuntimeBuilder.ALL_BUILDER_CLASSES.items():
        if cls.prefix is not None and name.startswith(cls.prefix):
            return cls(config, name)

    return None

class SetupPyBasedBuild(RuntimeBuilder):
    """ A builder for python packages that use distutil's setup.py """
    
    tarball_url = None

    _separate_debug_runtime_dir = True
    _separate_pyversion_runtime_dir = True
    
    def __init__(self, config):
        
        RuntimeBuilder.__init__(self, config)
        
        self.tarball_url = self.GetOption('source-tarball-url', 
                                          default=self.tarball_url)
        self.local_dist_name = self.tarball_url.split('/')[-1]
        self.dist_file = os.path.join(self.GetOption('root'), self.name,
                                      self.local_dist_name)

        target_python = self._GetTargetPython().proj_name
        self._dependencies = [target_python]
        self._runtime_dir_suffixes = [self._GetTargetPython().runtime_suffix]

    def _GetSrcDir(self):

        local_dirname = utils.StripExt(self.local_dist_name)
        src_dir = os.path.join(self.GetOption('root'), self.name,
                               local_dirname)
        return src_dir

    def Build(self):

        self._DownloadSource()
            
        build_dir = self._GetSrcDir()
        runtime_dir = self._GetRuntimeDir()
        target_python = self._GetTargetPython()

        if not self._GetDebugBuild():
            self._RunSetupPy(['build'])
        else:
            self._RunSetupPy(['build', '--debug'])
            
        if not os.path.exists(runtime_dir):
            os.makedirs(runtime_dir)
        site_pkgs_dir = runtime_dir + '/' + target_python.relative_site_packages
        if not os.path.exists(site_pkgs_dir):
            os.makedirs(site_pkgs_dir)
            
        self._RunSetupPy(['install', '--skip-build',
                          '--prefix=%s' % runtime_dir])
        
    def _RunSetupPy(self, setup_argv, set_win32_sdk_env=False):
        
        build_dir = self._GetSrcDir()
        runtime_dir = self._GetRuntimeDir()
        target_python = self._GetTargetPython()

        if not set_win32_sdk_env:
            env = None
        else:
            env = os.environ.copy()
            env['MSSDK'] = '1'
            env['DISTUTILS_USE_SDK'] = '1'

        cmdv = [target_python.executable, 'setup.py'] + list(setup_argv)
        self._RunArgvInEnv(cmdv, build_dir, env=env)
        
    def Clean(self):

        build_dir = self._GetSrcDir() + '/build'
        if os.path.isdir(build_dir):
            shutil.rmtree(build_dir)
    
    def _GetRepositoryInfo(self):
    
        info = utils.RetrievalOperations.Info()
        info.kind = 'tarball'
        info.url = self.tarball_url
        return info


class CythonBuild(SetupPyBasedBuild):
    
    name = 'cython'

    tarball_url = 'http://cython.org/release/Cython-0.20.1.tar.gz'

    def __init__(self, config):

        SetupPyBasedBuild.__init__(self, config)


RuntimeBuilder._RegisterSubClass(CythonBuild)
        
class PyZeroMqBuild(SetupPyBasedBuild):
    
    name = 'pyzeromq'
    
    tarball_url = 'https://pypi.python.org/packages/source/p/pyzmq/pyzmq-14.0.1.tar.gz'

    def __init__(self, config):

        SetupPyBasedBuild.__init__(self, config)
        self._dependencies += ['zeromq', 'cython']

    def Build(self):

        self._DownloadSource()
            
        build_dir = self._GetSrcDir()
        runtime_dir = self._GetRuntimeDir()

        zmq = GetBuilderForName('zeromq', self.config)
        zmq_dir = zmq._GetRuntimeDir()

        if not self._GetDebugBuild():
            self._RunSetupPy(['build', '--zmq=%s' % zmq_dir])
        else:
            self._RunSetupPy(['build', '--debug', '--zmq=%s' % zmq_dir])
        self._RunSetupPy(['install', '--skip-build',
                          '--prefix=%s' % runtime_dir])

RuntimeBuilder._RegisterSubClass(PyZeroMqBuild)

class IPythonBuild(SetupPyBasedBuild):
    
    name = 'ipython'
    
    tarball_url = 'https://pypi.python.org/packages/source/i/ipython/ipython-1.2.1.tar.gz'

    def __init__(self, config):

        SetupPyBasedBuild.__init__(self, config)
        self._dependencies += ['zeromq', 'pyzeromq']

RuntimeBuilder._RegisterSubClass(IPythonBuild)

class ConfiguredMakefileBuild(RuntimeBuilder):
    """ Builder for traditional unix-style configure; make distributions.
    Likely not to work on win32 w/o customization """
    
    tarball_url = None
    
    def __init__(self, config):

        RuntimeBuilder.__init__(self, config)


        self.url = self.GetOption('source-tarball-url', 
                                  default=self.tarball_url)
        self.local_dist_name = self.url.split('/')[-1]
        self.dist_file = os.path.join(self.GetOption('root'), self.name,
                                      self.local_dist_name)
        
    def _GetRepositoryInfo(self):
    
        info = utils.RetrievalOperations.Info()
        info.kind = 'tarball'
        info.url = self.url
        return info
        
    def _GetSrcDir(self):

        local_dirname = utils.StripExt(self.local_dist_name)
        src_dir = os.path.join(self.GetOption('root'), self.name,
                               local_dirname)
        return src_dir
    
    def _RunConfigure(self, argv=(), append_prefix=True):
        
        runtime_dir = self._GetRuntimeDir()
        src_dir = self._GetSrcDir()
        conf_argv = ['sh', 'configure'] + list(argv)
        if append_prefix:
            conf_argv += ['--prefix=%s' % runtime_dir.replace('/', os.sep)]
            
        extra = self.GetOption('extra-configure-args')
        if extra is not None and extra.strip() != '':
            conf_argv += [a.strip() for a in extra.split()]

        if self._GetDebugBuild():
            extra = self.GetOption('extra-configure-debug-args')
            if extra is not None and extra.strip() != '':
                conf_argv += [a.strip() for a in extra.split()]

        self._RunArgvInEnv(conf_argv, rundir=src_dir)

    def Build(self, config_creates='Makefile'):
        
        from_dist = self.GetOption('copy-from-distribution')
        if from_dist is not None:
            self._CopyFromDistribution()
            return
        
        self._DownloadSource()

        src_dir = self._GetSrcDir()
        if not os.path.exists(src_dir + '/' + config_creates):
            self._RunConfigure()
        
        make_argv = ['make', '-j2']
        self._RunArgvInEnv(make_argv, src_dir)
        make_argv = ['make', 'install']
        self._RunArgvInEnv(make_argv, src_dir)

    def Clean(self):
        
        src_dir = self._GetSrcDir()
        if not os.path.exists(src_dir + '/Makefile'):
            return
        
        make_argv = ['make', 'distclean']
        self._RunArgvInEnv(make_argv, src_dir)
        
    
class ZeroMqBuild(ConfiguredMakefileBuild):
    name = 'zeromq'

    tarball_url = 'http://download.zeromq.org/zeromq-4.0.4.tar.gz'
    
    _separate_debug_runtime_dir = True
    
    
    def __init__(self, config):

        ConfiguredMakefileBuild.__init__(self, config)
        

    def _RunConfigure(self, argv=(), append_prefix=True):

        if self._GetDebugBuild():
            argv = list(argv) + ['--enable-debug']
            
        ConfiguredMakefileBuild._RunConfigure(self, argv, append_prefix)
        
RuntimeBuilder._RegisterSubClass(ZeroMqBuild)

class GStreamerBuild(ConfiguredMakefileBuild):
    name = 'gstreamer0.10'
    _separate_debug_runtime_dir = True
    
RuntimeBuilder._RegisterSubClass(GStreamerBuild)

class GStreamerPluginsBuild(ConfiguredMakefileBuild):
    name = 'gstreamer-plugins-base0.10'
    _dependencies = [GStreamerBuild.name]
    _separate_debug_runtime_dir = True
    
RuntimeBuilder._RegisterSubClass(GStreamerPluginsBuild)

class LibXkbCommonBuild(ConfiguredMakefileBuild):
    name = 'libxkbcommon'
    _separate_debug_runtime_dir = True
    
RuntimeBuilder._RegisterSubClass(LibXkbCommonBuild)

class ICUBuilder(ConfiguredMakefileBuild):
    name = 'icu'
    _separate_debug_runtime_dir = True
    
    def _UsingWin32BinaryDist(self):
        
        return (self.GetOption('win32-binary-dist') == 'yes')

    def _GetSrcDir(self):

        top_dir = ConfiguredMakefileBuild._GetSrcDir(self)
        if self._UsingWin32BinaryDist():
            return top_dir
        return os.path.join(top_dir, 'source')
    
    def _DownloadSource(self):
        
        src_dir = ConfiguredMakefileBuild._GetSrcDir(self)
        if os.path.exists(src_dir):
            return
        
        info = self._GetRepositoryInfo()
        retr = utils.RetrievalOperations(self.config)
        retr.RetrieveIntoDir(info, src_dir)
        
    def Build(self, config_creates='Makefile'):
        
        if sys.platform == 'win32':
            self._BuildWin32()
            return
        
        super(ICUBuilder, self).Build(config_creates=config_creates)
    
    def _BuildWin32(self):

        raise NotImplementedError('win32 building with visualstudio does not'
                                  + ' seem to work as of version 64.2')
        self._DownloadSource()
        src_dir = self._GetSrcDir()
        rt_dir = self._GetRuntimeDir()

        if self.GetOption('win32-binary-dist') == 'yes':
            utils.Install(src_dir, rt_dir)
            return
        
        cmdv = ['msbuild', r'allinone\allinone.sln',
                '/p:Configuration=Debug', '/p:Platform=Win32', '/p:SkipUWP=true']
        self._RunArgvInEnv(cmdv, src_dir)

RuntimeBuilder._RegisterSubClass(ICUBuilder)


class PipBasedBuild(RuntimeBuilder):
    """ Basic support for creating runtimes via pip.  Runs pip install to
    install into runtime dir so it can be packaged up
    
    Name start with 'pip-with-' followed by pip package names separated by -
    """
    
    _separate_debug_runtime_dir = True

    prefix = 'pip-with-'
    
    def __init__(self, config, name=None):
        
        RuntimeBuilder.__init__(self, config, name=name)
        
        rest = name[len(self.prefix):]
        self.pip_packages = rest.split('-')
        
        target_python = self._GetTargetPython().proj_name
        self._dependencies = [target_python]
        self._runtime_dir_suffixes = [self._GetTargetPython().runtime_suffix]

    def _GetSrcDir(self):

        src_dir = os.path.join(self.GetOption('root'), self.name)
        return src_dir

    def Build(self):

        build_dir = self._GetSrcDir()
        runtime_dir = self._GetRuntimeDir()

        if not os.path.isdir(build_dir):
            os.makedirs(build_dir)
        target_python = self._GetTargetPython()

        get_pip_url = 'https://bootstrap.pypa.io/get-pip.py'
        get_pip_py = build_dir + '/get-pip.py'
        if not os.path.exists(get_pip_py):

            retr = utils.RetrievalOperations(self.config)
            retr.Download(get_pip_url, get_pip_py)
        
        site_pkgs_dir = runtime_dir + '/' + target_python.relative_site_packages
        
        get_pip_argv = [target_python.executable, get_pip_py,
                        '--build', build_dir, '--target', site_pkgs_dir]
        if not os.path.isdir(site_pkgs_dir+'/pip'):
            self._RunArgvInEnv(get_pip_argv, build_dir)

        pip_install_argv = ['install'] + self.pip_packages + ['--prefix', runtime_dir]
        cmdv = [target_python.executable, '-m', 'pip'] + pip_install_argv
        self._RunArgvInEnv(cmdv, build_dir)
        
    def Clean(self):

        build_dir = self._GetSrcDir() + '/build'
        if os.path.isdir(build_dir):
            shutil.rmtree(build_dir)

RuntimeBuilder._RegisterSubClass(PipBasedBuild)

class PipInstalledPackage(RuntimeBuilder):
    """ Basic support for creating runtimes via pip.  Runs pip install to
    install into runtime dir so it can be packaged up
    
    Name start with 'pip-installed-' followed by pip package names separated by -
    """
    
    _separate_debug_runtime_dir = True

    prefix = 'pip-installed-'
    
    def __init__(self, config, name=None):
        
        RuntimeBuilder.__init__(self, config, name=name)
        
        if self.name.startswith(self.prefix):
            rest = self.name[len(self.prefix):]
        else:
            rest = self.name
        self.pip_packages = rest.split('-')
        
        target_python = self._GetTargetPython()
        py_builder = GetBuilderForName(target_python.proj_name, self.config)
        
        self._dependencies = [target_python.proj_name] + list(py_builder._dependencies)
        self._runtime_dir_suffixes = [target_python.runtime_suffix]

    def _GetSrcDir(self):

        src_dir = os.path.join(self.GetOption('root'), self.name)
        return src_dir

    def Build(self):

        src_dir = self._GetSrcDir()
        build_dir = src_dir + '/build'
        tmp_pip_install = src_dir + '/tmp-pip'
        runtime_dir = self._GetRuntimeDir()

        if not os.path.isdir(build_dir):
            os.makedirs(build_dir)
        target_python = self._GetTargetPython()

        tmp_pip_env = None
        
        use_get_pip = False
        try:
            cmdv = [target_python.executable, '-m', 'pip', '--version']
            self._RunArgvInEnv(cmdv, build_dir, env=tmp_pip_env)
        except:
            use_get_pip = True
            
        if use_get_pip:
            
            get_pip_url = 'https://bootstrap.pypa.io/get-pip.py'
            get_pip_py = src_dir + '/get-pip.py'
            if not os.path.exists(get_pip_py):
    
                retr = utils.RetrievalOperations(self.config)
                retr.Download(get_pip_url, get_pip_py)
            
            get_pip_argv = [target_python.executable, get_pip_py,
                            '--build', build_dir, '--target', site_pkgs_dir]
            site_pkgs_dir = tmp_pip_install + '/' + target_python.relative_site_packages
            if not os.path.isdir(site_pkgs_dir+'/pip'):
                self._RunArgvInEnv(get_pip_argv, build_dir)

            tmp_pip_env = {'PYTHONPATH': site_pkgs_dir}

        pip_install_argv = ['install'] + self.pip_packages + ['--prefix', runtime_dir]
        cmdv = [target_python.executable, '-m', 'pip'] + pip_install_argv
        self._RunArgvInEnv(cmdv, build_dir, env=tmp_pip_env)
        
    def Clean(self):

        build_dir = self._GetSrcDir() + '/build'
        if os.path.isdir(build_dir):
            shutil.rmtree(build_dir)

RuntimeBuilder._RegisterSubClass(PipInstalledPackage)

class PyCurlBuilder(SetupPyBasedBuild):
    name = 'pycurl'

    _dependencies = ['curl']
    
    def Build(self):

        curl_builder = GetBuilderForName('curl', self.config)

        self._DownloadSource()
            
        build_dir = self._GetSrcDir()
        runtime_dir = self._GetRuntimeDir()

        curl_rt_dir = curl_builder._GetRuntimeDir()
        if sys.platform == 'win32':
            def find_libname():
                for name in glob.glob(curl_rt_dir+'/lib/libcurl*.lib'):
                    return os.path.basename(name)
                return 'libcurl.lib'
            curl_args_for_setup = ['--curl-dir={}'.format(curl_rt_dir), 
                                   '--libcurl-lib-name={}'.format(find_libname())]
        else:
            curl_config_filename = '{}/bin/curl-config'.format(curl_rt_dir)
            curl_args_for_setup = ['--curl-config={}'.format(curl_config_filename)]

        if not self._GetDebugBuild():
            self._RunSetupPy(['build'] + curl_args_for_setup, set_win32_sdk_env=True)
        else:
            self._RunSetupPy(['build', '--debug'] + curl_args_for_setup, set_win32_sdk_env=True)
            
        self._RunSetupPy(['install', '--skip-build',
                          '--prefix=%s' % runtime_dir] + curl_args_for_setup)
    
RuntimeBuilder._RegisterSubClass(PyCurlBuilder)


class CurlBuilder(ConfiguredMakefileBuild):
    """ Builds on win32 as well, which does not use configure """
    
    name = 'curl'

    _separate_debug_runtime_dir = True
    
    def Build(self):
        
        if sys.platform == "win32":
            return self._BuildWin32()
        ConfiguredMakefileBuild.Build(self, 'config.status')

    def Clean(self):
        
        if sys.platform == "win32":
            return self._CleanWin32()
        ConfiguredMakefileBuild.Clean(self)
    
    def _BuildWin32(self):
        
        self._DownloadSource()
        
        build_dir = self._GetSrcDir()

        build_cmdv = ['nmake', '/f', 'Makefile.vc', 'mode=static']
        if self._GetDebugBuild():
            build_cmdv.append('DEBUG=yes')
            
        self._RunArgvInEnv(build_cmdv, build_dir+'/winbuild')

        self._InstallWin32()

    def _InstallWin32(self):
        
        src_dir = self._GetSrcDir()
        runtime_dir = self._GetRuntimeDir()
        
        def find_in_builds():
            
            build_type = ('-debug-' if self._GetDebugBuild() else '-release-')
            
            name_list = [name for name in glob.glob(src_dir+'/builds/*')
                         if build_type in name and '-obj-' not in name]
            return name_list[0]
        
        built_dir = find_in_builds()
        print built_dir
        utils.Install(built_dir, runtime_dir)
            
    def _CleanWin32(self):

        src_dir = self._GetSrcDir()
        if not os.path.exists(src_dir):
            return
        
        builds_dir = src_dir + '/builds'
        for name in glob.glob(builds_dir+'/*'):
            if os.path.isdir(name):
                shutil.rmtree(name)
            else:
                os.remove(name)
        
RuntimeBuilder._RegisterSubClass(CurlBuilder)

class OpenSSLBuilder(ConfiguredMakefileBuild):
    
    name = 'openssl'

    _separate_debug_runtime_dir = True

    def Build(self, config_creates='config-has-been-run'):
        
        if sys.platform == 'win32':
            raise NotImplementedError('building openssl not supported on win32')
        
        ConfiguredMakefileBuild.Build(self, config_creates=config_creates)
        
    def Clean(self):
        
        ConfiguredMakefileBuild.Clean(self)
        
        src_dir = self._GetSrcDir()
        config_has_been_run = src_dir + '/config-has-been-run'
        if os.path.exists(config_has_been_run):
            os.remove(config_has_been_run)
            
    def _RunConfigure(self, argv=(), append_prefix=True):
        
        runtime_dir = self._GetRuntimeDir()
        src_dir = self._GetSrcDir()
        conf_argv = ['sh', 'config'] + list(argv)
        if append_prefix:
            conf_argv += ['--prefix=%s' % runtime_dir.replace('/', os.sep), 
                          '--openssldir=%s/ssl' % runtime_dir]
            
        extra = self.GetOption('extra-configure-args')
        if extra is not None and extra.strip() != '':
            conf_argv += [a.strip() for a in extra.split()]

        if self._GetDebugBuild():
            extra = self.GetOption('extra-configure-debug-args')
            if extra is not None and extra.strip() != '':
                conf_argv += [a.strip() for a in extra.split()]

        self._RunArgvInEnv(conf_argv, rundir=src_dir)

        f = open(os.path.join(src_dir, 'config-has-been-run'), 'w')
        f.write("Args: %s\n" + repr(conf_argv))
        f.close()
        

OpenSSLBuilder._RegisterSubClass(OpenSSLBuilder)

class PyICUBuild(SetupPyBasedBuild):
    
    name = 'pyicu'
    tarball_url = 'pyicu-from-pip'

    def __init__(self, config):

        super(PyICUBuild, self).__init__(config)
        self._dependencies += ['icu', 'openssl']

    def _GetEnvChanges(self, base_ok=False):
        
        changes = super(PyICUBuild, self)._GetEnvChanges()
        if sys.platform == 'win32':
            icu_builder = GetBuilderForName('icu', self.config)
            icu_rt_dir = icu_builder._GetRuntimeDir()
            print changes
            changes['ICU_VERSION'] = '64.2'
            changes['MSSDK'] = '1'
            changes['DISTUTILS_USE_SDK'] = '1'
            changes['PYICU_INCLUDES'] = icu_rt_dir+'/include'
            changes['PYICU_LFLAGS'] = '/LIBPATH:' + icu_rt_dir + '/lib'
            
        return changes
    
    def UpdateSource(self):
        
        proj_dir = os.path.join(self.GetOption('root'), self.name)
        if not os.path.exists(proj_dir):
            os.makedirs(proj_dir)
            
        target_python = self._GetTargetPython()

        pip_name = self.GetOption('pip-package', default=self.name)
        pip_version = self.GetOption('pip-version', default=None)

        filename_list = glob.glob(proj_dir + '/*.tar.gz')
        if len(filename_list) == 0:
            package_arg = pip_name
            if pip_version is not None:
                package_arg += '==' + pip_version
            pip_argv = ['download', package_arg]
            cmdv = [target_python.executable, '-m', 'pip'] + pip_argv
            self._RunArgvInEnv(cmdv, proj_dir)
        
            filename_list = glob.glob(proj_dir + '/*.tar.gz')
            if len(filename_list) != 1:
                raise ValueError('More than one .tar.gz file: ' + repr(filename_list))
        
        src_dir = self._GetSrcDir()
        if not os.path.exists(src_dir):
            filename = filename_list[0]
            utils.UnpackTar(filename, src_dir)
            self._ApplyPatch()

    def _GetSrcDir(self):
        
        proj_dir = os.path.join(self.GetOption('root'), self.name)
        filename_list = glob.glob(proj_dir + '/*.tar.gz')
        if len(filename_list) != 1:
            raise ValueError('Number of .tar.gz files is not one: ' + repr(filename_list))
        
        filename = filename_list[0]
        return utils.StripExt(filename)
            
    def Build(self):
        
        self.UpdateSource()

        src_dir = self._GetSrcDir()
        proj_dir = os.path.dirname(src_dir)
        tmp_build_dir = os.path.join(proj_dir, 'tmp-build')
        runtime_dir = self._GetRuntimeDir()
        target_python = self._GetTargetPython()
        
        verbose = True

        pip_argv = ['install', src_dir,
                    '--build', tmp_build_dir,
                    '--prefix', runtime_dir]
        if self._GetDebugBuild():
            pip_argv += ['--global-option', 'build', '--global-option', '--debug']
        if verbose:
            pip_argv += ['--verbose']
        cmdv = [target_python.executable, '-m', 'pip'] + pip_argv
        self._RunArgvInEnv(cmdv, proj_dir)
        
    def Clean(self):
        
        proj_dir = os.path.join(self.GetOption('root'), self.name)
        tmp_build_dir = os.path.join(proj_dir, 'tmp-build')
        if os.path.exists(tmp_build_dir):
            shutil.rmtree(tmp_build_dir)
        
RuntimeBuilder._RegisterSubClass(PyICUBuild)
