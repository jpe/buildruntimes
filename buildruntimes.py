# Copyright (c) 2001-2012, Archaeopteryx Software, Inc.  All rights reserved.
#
# Written by John P. Ehresman and Stephan R.A. Deibel
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


""" Build script for qt and pyside and dependencies


"""

import os.path
import sys

def Main(argv):
    
    parent_dir = os.path.dirname(os.path.abspath(__file__))
    src_dir = os.path.join(parent_dir, 'src')
    arg0_fullname = os.path.abspath(sys.argv[0])
    root = os.path.dirname(os.path.dirname(arg0_fullname))

    sys.path.insert(0, src_dir)
    import buildruntimes.cmds

    return buildruntimes.cmds.Main(argv, root=root)

if __name__ == '__main__':
    r = Main(sys.argv)
    sys.exit(0 if r is None else r)
