This is build system for relocatable binary runtimes, with 
explicit support for building debug enabled versions to support
development.  It's evolving from a script to build pyside and
its dependencies on systems without having to install too many 
prerequisites.

Thare are a few prerequisites, though -- they are:
  git (installed in c:\Program Files\Git on win32)
  perl (installed in c:\Perl on win32)
  C/C++ build environment (VS 2008 for win32, XCode for OS X, g++ etc for Linux)
  See alao Windows notes below

Files are downloaded, directories created, etc under a root build directory,
which defaults to the parent directory of the buildruntimes directory.
All packages are built for and installed into their own runtime directory
under $root/runtimes.  The runtime directories are named with their
project name and then -debug or -release if separate directories
are needed for debug / release.

The script can build the following:
  python2.7 (not on win32 yet)
  python3.2 (not on win32 yet)
  python3.3 (not on win32 yet)
  cmake (not on win32 yet)
  qt
  shiboken (needs python, cmake, qt)
  pyside
  
The commands to build a python 2.7 based series of runtimes are:
  buildruntimes.py build python2.7
  buildruntimes.py build cmake
  buildruntimes.py build qt
  buildruntimes.py build shiboken
  buildruntimes.py build pyside
Note that qt is a large project and takes quite a while to clone and then
to build.  You also must type 'yes' manually to accept the LGPL license
conditions when configuring.
  
Commands:
  build -- builds the project after cloning the source if the source directory
           does not exist. Options:
             --target-python=pythonX.Y may be used to specify a python version 
                 (python2.7 is the default)
             --debug=no may be used for a release build (debug build is the 
                 default)
             --base-runtime-dirs=<directory> may be used to specify root
                 directory for base runtimes
  clean -- deletes all object and other intermediate files from the build
           tree (but not the install tree)
  shell -- launch a shell with the correct environment values set
  archive -- archive runtime files in a .tar.bz2 file (archive build files
             if --include-debugfiles=yes is specified)
  expand -- expand runtime files from a .tar.bz2 file (expand build files
            if --include-debugfiles=yes is specified)

Windows notes:

CMake, git, and python need to be built and installed separately and only
Python 2.7 is currently supported.  The default install directory for CMake
is C:\Program Files\CMake 2.8  On 64 bit versions of Windows,
C:\Program Files (x86) may be used for install locations rather than
C:\Program Files

The script will run build commands in the correct environment for VS 2008.
